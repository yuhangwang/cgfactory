#====================================================
## ::cgfactory::visual pbbc
# Goal: visualize PBBC bonds correctly
# Author: Yuhang Wang
# Date: 11/23/2014

#====================================================

proc ::cgfactory::visual::show_pbbc { molId selection_str max_bond_length } { 
	## Role: correctly visualize PBBC
	#  Inputs:
	#	molId: VMD mol ID
	#	selection_str: atom selection string for the target atom set
	#	max_bond_length: maximum bond length
	#
	set target_atoms [atomselect $molId $selection_str]
	set atomIds_target_atoms [$target_atoms get index]
	set all_long_bonds {} 
	foreach _atomid_target $atomIds_target_atoms {
        	set _target [ atomselect $molId "index $_atomid_target" ] 
        	set _atomIds_neighbor [ lindex [$_target getbonds] 0 ] 
        	if { [ llength $_atomIds_neighbor ] > 0 } { 
			set _long_bonds [::cgfactory::tools::topology::find_long_bond \
			 	$molId $_atomid_target $_atomIds_neighbor $max_bond_length]
			if {[llength $_long_bonds] >0} { ;# if not empty
				set all_long_bonds [concat $all_long_bonds $_long_bonds]
			}
        	} 
		$_target delete
	} 
	
	## wrap long-bonds and re-draw them using lines
	#  Developer's note:
	#  No need to remove duplicated pairs, i.e. {1 2} and {2 1} are different bonds,
	#  because under PBC condition, 1--2' and 2--1', i.e. they all
	#  pair with their neighbor's images.
	::cgfactory::visual::draw_pbbc_bonds $molId $all_long_bonds
	
	$target_atoms delete
	return
}

proc ::cgfactory::visual::draw_pbbc_bonds {molId long_bond_pairs {style "dashed"} {width 1} } {
	## Role: do PBC-wrapping and then draw the bonds
	#  Inputs:
	#	molId: VMD mol ID
	#	long_bonds: a list of lists. 
	#		each element should have 2 items: index_atom1 index_atom2
	#	style: "dashed"(default) or "solid"
	#	width: line width (default 1)
	#
	foreach id_pair $long_bond_pairs {
		lassign $id_pair _atomid1 _atomid2
		lassign [::cgfactory::tools::pbc::get_wrapped_coordinates $molId $_atomid1 $_atomid2] _coord1 _coord2
		::cgfactory::visual::draw_line $molId $_coord1 $_coord2 $style $width
		
		set d [vecdist $_coord1 $_coord2]
	}
} 


proc ::cgfactory::visual::draw_line {molId coord1 coord2 {style "dashed"} {width 1} {color "cyan"} } {
	## Role: draw a line connecting coord1 and coord2
	#  Inputs:
	#	molId: VMD mol ID
	#	coord1: coordinate of point 1
	#	coord2: coordinate of point 2
	#	style: "dashed"(default) or "solid"
	#	width: line width (default 1)
	#
	draw color $color
	graphics $molId line $coord1 $coord2 style $style width $width
	
}