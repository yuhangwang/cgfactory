#====================================================
## ::cgfactory::lipid:build
# Role: build differnt types of coarse-grained lipid molecules
# Available lipid types:
#	* POPC
# Author: Yuhang Wang
# Date: 11/19/2014

#====================================================

## define namespaces specific to this submodule
source [file join [::cgfactory::get_lipid_dir] "lipid_helper_tools.tcl"]
source [file join [::cgfactory::get_lipid_dir] "lipid_make_pc_head.tcl"]
source [file join [::cgfactory::get_lipid_dir] "lipid_make_tail.tcl"]



proc ::cgfactory::lipid::build_dppc {{resid 1} {segname "PC"} {chainId "L"} } {
	## build a CG POPC molecule
	
	set molIdList {}
	set resname "PC"
	
	## make PC head
	lassign [::cgfactory::lipid::make_pc_head $resid $resname $segname $chainId] molId Lx Ly Lz
	## move PC head group up by half head-bond length
	lassign {0 0 0} dx dy dz
	set angle_TailHeadTail $::cgfactory::lipid::ANGLE_TailHeadTail ;# angle for lipid tail-head-tail
	set dy [expr cos($angle_TailHeadTail/2.0)*$::cgfactory::lipid::DEFAULT_HEAD_BOND_LENGTH]
	puts "== dy = $dy ; cos(t) = [expr cos($angle_TailHeadTail/2.0)]"
	::cgfactory::tools::coordinate::move $molId $dx $dy $dz
	lappend molIdList $molId

	set n_beads 5
	
	## make left tail
	puts "make left tail ..."
	set leftRight -1
	lassign [::cgfactory::lipid::make_tail $n_beads  $resid $resname $segname $chainId $leftRight] molId Lx Ly Lz
	lappend molIdList $molId
	
	## make right tail
	puts "make right tail ..."
	set leftRight 1
	lassign [::cgfactory::lipid::make_tail $n_beads  $resid $resname $segname $chainId $leftRight] molId Lx Ly Lz
	lappend molIdList $molId
	
	## merge all
	set molId [::cgfactory::tools::molecule::mergemols $molIdList]
	
	## garbage collect
	::cgfactory::tools::molecule::delete_mols $molIdList
	
	## add bonds
	::cgfactory::tools::topology::guess_bonds $molId 
	
	## add angles
	::cgfactory::tools::topology::guess_angles $molId
	
	## Do NOT add dihedrals
	# in DPD, lipids don't have dihedral terms
	#::cgfactory::tools::topology::guess_dihedrals $molId
	
	# add an additional bond between TL1 and TR1
#	set sel1 [atomselect $molId "name TL1"]
#	set sel2 [atomselect $molId "name TR1"]
#	set id1 [$sel1 get index]
#	set id2 [$sel2 get index]
#	$sel1 delete
#	$sel2 delete
#	set bondtype "TL1-TR1"
#	::cgfactory::tools::topology::add_bond $molId $id1 $id2 $bondtype
	
	## set new PBC box size
	set x_margin [expr 5.0]
	set y_margin [expr 5.0]
	set z_margin [expr 5.0]
	::cgfactory::tools::pbc::smart_set_box_size $molId $x_margin $y_margin $z_margin
	
	return $molId
}