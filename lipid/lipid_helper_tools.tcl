#====================================================
## ::cgfactory::lipid::helper_tools
# Goal: provoide helper tools for building lipids
# Author: Yuhang Wang
# Date: 11/19/2014

#====================================================


proc ::cgfactory::lipid::tail_bead_unit_box_size {} {
	## Role: return the size of the unit box per bead
	set bond_length $::cgfactory::lipid::TAIL_CC_BOND_LENGTH
	set Lx [expr $bond_length] 
	set Ly [expr $bond_length]
	set Lz [expr $bond_length]
	return [list $Lx $Ly $Lz]
}

proc ::cgfactory::lipid::delete_extra_angles {molId} {
	## Role: delete extra angles added by topo guess_angles
	set angle_list [::cgfactory::tools::topology::get_angle_list $molId]
	set exclusion_list $::cgfactory::lipid::excluded_angle_types
	puts "=== $exclusion_list ==="
	foreach entry $angle_list {
		lassign $entry _type _id1 _id2 _id3
		puts "=== $_type ==="
		if {[lsearch $exclusion_list $_type] != -1 } {
			puts " ===  delete $_type ==="
			::cgfactory::tools::topology::delete_angle $molId $_id1 $_id2 $_id3
		}
	}
	return
}