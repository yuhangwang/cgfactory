
#====================================================
## ::cgfactory::lipid namespace
# Goal: define lipid's namespace
# Author: Yuhang Wang

#====================================================
namespace eval ::cgfactory::lipid {
	variable N_CARBONS_PER_TAIL_BEAD [expr 3]
	variable N_PC_HEAD_BEADS	 [expr 3] ;#< number of PE head beads
	variable TAIL_CC_BOND_LENGTH 	 [expr 3.5]
	variable DEFAULT_HEAD_BOND_LENGTH 	 [expr 4.0]
	variable Pi [expr 3.1415926]
	variable ANGLE_TailHeadTail	 [expr $Pi/2.0]
	variable excluded_angle_types {}
}


namespace eval ::cgfactory::lipid::BOND {
	## bond length
	variable bondLengthDict {
		"PC1-PC2" 4.0
		"PC2-PC3" 4.8
		"PC3-TR1" 4.1
		"PC3-T31" 4.2
		"T-T"	  3.5
	}
}
namespace eval ::cgfactory::lipid::TYPE {
	# Role: define conventions for atom types, names in CG graphene
	variable atomTypeDict {
		"PC1" "PC1"
		"PC2" "PC2"
		"PC3" "PC3"
		"TL1" "TL1"
		"TL2" "TL2"
		"TL3" "TL3"
		"TL4" "TL4"
		"TL5" "TL5"
		"TR1" "TR1"
		"TR2" "TR2"
		"TR3" "TR3"
		"TR4" "TR4"
		"TR5" "TR5"
	}
}

namespace eval ::cgfactory::lipid::NAME {
	# Role: define conventions for atom types, names in CG graphene
	variable atomNameDict {
		"PC1" "PC1"
		"PC2" "PC2"
		"PC3" "PC3"
		"TL1" "TL1"
		"TL2" "TL2"
		"TL3" "TL3"
		"TL4" "TL4"
		"TL5" "TL5"
		"TR1" "TR1"
		"TR2" "TR2"
		"TR3" "TR3"
		"TR4" "TR4"
		"TR5" "TR5"
	}
}

namespace eval ::cgfactory::lipid::MASS {
	# Role: conventions for atom masses
	# note: all masses are in AKMA units
	set massLPT 42 ;# C3H6
	set massPC1 89 ;# NC4H11
	set massPC2 109 ;# CH2PO4
	set massPC3 129 ;# C5H5O4
	variable atomMassDict [list \
		"PC1" $massPC1 \
		"PC2" $massPC2 \
		"PC3" $massPC3 \
		"TL1" $massLPT \
		"TL2" $massLPT \
		"TL3" $massLPT \
		"TL4" $massLPT \
		"TL5" $massLPT \
		"TR1" $massLPT \
		"TR2" $massLPT \
		"TR3" $massLPT \
		"TR4" $massLPT \
		"TR5" $massLPT \
	]
}

namespace eval ::cgfactory::lipid::CHARGE {
	# Role: conventions for atom masses
	variable atomChargeDict {
		"PC1"  0.0
		"PC2"  0.0
		"PC3"  0.0
		"TL1"  0.0
		"TL2"  0.0
		"TL3"  0.0
		"TL4"  0.0
		"TL5"  0.0
		"TR1"  0.0
		"TR2"  0.0
		"TR3"  0.0
		"TR4"  0.0
		"TR5"  0.0
	}
}


