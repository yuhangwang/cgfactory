#====================================================
## ::cgfactory::lipid::make_one_bead
# Goal: make one PE head bead
# Author: Yuhang Wang
# udpated: 03-07-15

#====================================================
proc ::cgfactory::lipid::make_one_bead { beadName beadType beadMass beadCharge {beadRadius 4.0} } {
	## Role: build a single CG bead
	# note: VMD will add a bond for two atoms A and B
	#	if dAB < 0.6*(R_A + R_B)
	# 	where dAB is the distance between A and B
	#	R_A and R_B are radius of A and B, respectively.
	#	Set default radius = 4.0 seems to be a good choice in order for guess_bond to work properly
	# see: https://sites.google.com/site/akohlmey/software/topotools/topotools-tutorial---part-1
	set molId [::cgfactory::tools::make::make_one_bead]
	set oneAtom [atomselect $molId all]
	$oneAtom set name   $beadName
	$oneAtom set type   $beadType
	$oneAtom set mass   $beadMass
	$oneAtom set charge $beadCharge
	$oneAtom set radius $beadRadius
	set L [expr $beadRadius*2.0]
	set Lx $L
	set Ly $L
	set Lz $L
	::cgfactory::tools::pbc::set_box_size $molId $Lx $Ly $Lz
	return [list $molId $Lx $Ly $Lz]
}