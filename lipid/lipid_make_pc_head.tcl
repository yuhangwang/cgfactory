#====================================================
## ::cgfactory::lipid:make_pc_head
# Role: make a PC head group
# Available lipid types:
#	* DPPC
# Author: Yuhang Wang
# Date: 11/19/2014

#====================================================
## Dependencies
source [file join [::cgfactory::get_lipid_dir] "make_one_bead.tcl"]

proc ::cgfactory::lipid::make_pc_head {new_resid new_resname new_segname new_chainId} {
	## Role: make a PE head group
	#  Inputs:
	#	new_resid: new residue ID
	#	new_resname: new residue name
	#	new_segname: new segment name
	#	new_chainId: new chain ID
	#
	set molIdList {} ;#< a list of molId's for each tail bead
	
	## define coordinate offset for each bead
	set dx 0
	set dy 0
	set dz 0
	
	## define PC head bead names
	set headNameList {"PC1" "PC2" "PC3"}
	
	## build one tail bead at a time
	for {set ccc 0} {$ccc < $::cgfactory::lipid::N_PC_HEAD_BEADS} {incr ccc} {
		set name [lindex $headNameList $ccc]
		set beadName [dict get $::cgfactory::lipid::NAME::atomNameDict $name]
		set beadType [dict get $::cgfactory::lipid::TYPE::atomTypeDict $name]
		set beadMass [dict get $::cgfactory::lipid::MASS::atomMassDict $name]
		set beadCharge [dict get $::cgfactory::lipid::CHARGE::atomChargeDict $name]
		
		lassign [::cgfactory::lipid::make_one_bead $beadName $beadType $beadMass $beadCharge] molId Lx Ly Lz
		
		# set resid, resname, segname and chainId
		set selection_str "all"
		::cgfactory::tools::param::set_residue_param  $molId $selection_str $new_resid $new_resname $new_segname $new_chainId
		
		## define coordinate offset along Y
		set dy [expr ($::cgfactory::lipid::N_PC_HEAD_BEADS-$ccc-1)*$::cgfactory::lipid::DEFAULT_HEAD_BOND_LENGTH]
		::cgfactory::tools::coordinate::move $molId $dx $dy $dz
		
		## enroll the new bead to the list
		lappend molIdList $molId
	}
	
	## combine beads into one molecules
	set newMolId [::cgfactory::tools::molecule::mergemols $molIdList]
	
	## garbage collect un-needed intermediate molecules
	::cgfactory::tools::molecule::delete_mols $molIdList
	
	## update box information
	lassign [::cgfactory::tools::pbc::get_box_size $newMolId] Lx Ly Lz
	
	## update box parameters for the new residue
	::cgfactory::tools::pbc::set_box_size $newMolId $Lx $Ly $Lz
	
	## make bonds
	::cgfactory::tools::topology::guess_bonds $newMolId
	
	## make angles
	::cgfactory::tools::topology::guess_angles $newMolId
	
	return [list $newMolId $Lx $Ly $Lz]	
}