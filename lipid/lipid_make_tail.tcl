#====================================================
## ::cgfactory::lipid::make_tail
# Goal: make one lipid tail
# Author: Yuhang Wang

#====================================================

## Dependencies
source [file join [::cgfactory::get_lipid_dir] "make_one_bead.tcl"]

proc ::cgfactory::lipid::make_tail {n_beads new_resid new_resname new_segname new_chainId leftRight} {
	## make a lipid tail with n beads
	#  Inputs:
	#	n_beads: number of beads
	#	new_resid: starting residue ID
	#	new_resname: residue name
	#	new_segname: new segment name
	#	leftRight: -1 for left tail; 1 for right tail; 0 for middle tail;
	#
	set molIdList {} ;#< a list of molId's for each tail bead
	
	## define coordinate offset for each bead
	set Pi 3.1415926
	set angle_TailHeadTail $::cgfactory::lipid::ANGLE_TailHeadTail ;# angle for lipid tail-head-tail
	set dx [expr $leftRight*$::cgfactory::lipid::TAIL_CC_BOND_LENGTH*sin($angle_TailHeadTail/2.0)]
	set dy 0
	set dz 0
	
	if {$leftRight == -1} {
		set beadNamePrefix "TL"
	} else {
		set beadNamePrefix "TR"
	}
	
	## build one tail bead at a time
	for {set ccc 0} {$ccc < $n_beads} {incr ccc} {
		set beadId [expr $ccc + 1]
		set name "${beadNamePrefix}${beadId}"
		set beadName [dict get $::cgfactory::lipid::NAME::atomNameDict $name]
		set beadType [dict get $::cgfactory::lipid::TYPE::atomTypeDict $name]
		set beadMass [dict get $::cgfactory::lipid::MASS::atomMassDict $name]
		set beadCharge [dict get $::cgfactory::lipid::CHARGE::atomChargeDict $name]
		lassign [::cgfactory::lipid::make_one_bead $beadName $beadType $beadMass $beadCharge] molId Lx Ly Lz
		set selection_str "all"
		::cgfactory::tools::param::set_residue_param  $molId $selection_str $new_resid $new_resname $new_segname $new_chainId
		
		## define coordinate offset along Y
		set dy [expr -1*$ccc*$::cgfactory::lipid::TAIL_CC_BOND_LENGTH]
		::cgfactory::tools::coordinate::move $molId $dx $dy $dz
		
		## enroll the new bead to the list
		lappend molIdList $molId
	}
	
	## combine beads into one molecules
	set newMolId [::cgfactory::tools::molecule::mergemols $molIdList]
	
	## garbage collect un-needed intermediate molecules
	::cgfactory::tools::molecule::delete_mols $molIdList
	
	## update box information
	lassign [::cgfactory::tools::pbc::get_box_size $newMolId] Lx Ly Lz
	
	## update box parameters for the new residue
	::cgfactory::tools::pbc::set_box_size $newMolId $Lx $Ly $Lz
	
	## make bonds
	::cgfactory::tools::topology::guess_bonds $newMolId
	
	## make angles
	::cgfactory::tools::topology::guess_angles $newMolId
	
	## make dihedrals
	## Note: there is no dihedral term in DPD  PE model
#	::cgfactory::tools::topology::guess_dihedrals $newMolId
	
	return [list $newMolId $Lx $Ly $Lz]
}