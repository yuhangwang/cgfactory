#====================================================
## ::cgfactory::oxidize::add_cg_hydroxyl
# Goal: add oxygen species to a target
# Author: Yuhang Wang
# Date: 11/20/2014

#====================================================
source [file join [::cgfactory::get_oxidize_dir] "oxidize_make_hydroxyl_bead.tcl"]
source [file join [::cgfactory::get_oxidize_dir] "oxidize_find_oxidation_sites.tcl"]

proc ::cgfactory::oxidize::mutate_to_hydrophilic {molId selection_str topBottom {oxy_level 1} } {
	## Role: add coarse-grained hydroxyl group to a CG bead
	#  Inputs:
	#	molId: VMD mol ID
	#	selection_str: VMD atom selection string
	#	topBottom: 1 for adding hydroxyl group to the top; -1 for adding to the bottom
	#
	set _target_set [atomselect $molId $selection_str]
	set n_atoms_selected [$_target_set num]
	
	set name_new_bead $::cgfactory::oxidize::NAME::OGB
	set type_new_bead $::cgfactory::oxidize::TYPE::OGB
	set mass_new_bead $::cgfactory::oxidize::MASS::OGB
	set charge_new_bead $::cgfactory::oxidize::CHARGE::OGB
	
	## if no atom selected, just return the original molId
	if {$n_atoms_selected == 0} {
		puts "WARNING: zero atom selected for oxidation; no oxidation done."
		return molId
	}
	set raw_atomIds [$_target_set get index]
	
	if {$oxy_level == 1 } {
		## no need to do random picking, if the user want to oxidize the whole atom selection
		set atomIds $raw_atomIds
	} else {
		set atomIds [::cgfactory::oxidize::find_oxidation_sites $raw_atomIds $oxy_level]
	}
	
	puts "======= select: $selection_str ====="
	puts "======= raw atomIds: $raw_atomIds ==== "
	puts "======= atoms to be oxidize: $atomIds ====="
	$_target_set delete
	
	## for each target atom, at a hydroxyl bead
	
	foreach _atomid $atomIds {
		puts "== mutate bead $_atomid into a hydrophilic ${name_new_bead} bead"
		## get target infomation
		set _sel [atomselect $molId "index $_atomid "]
		$_sel set name $name_new_bead
		$_sel set type $type_new_bead
		$_sel set mass $mass_new_bead
		$_sel set charge $charge_new_bead
		$_sel delete
	}
	
	return $molId
}
