#====================================================
## ::cgfactory::oxidize::make_hydroxyl_bead
# Goal: make a hydroxyl bead
# Author: Yuhang Wang
# Date: 11/20/2014

#====================================================

proc ::cgfactory::oxidize::make_hydroxyl_bead {resid resname chainid segname} {
	## Role: build a single hydroxyl group bead
	set molId [::cgfactory::tools::make::make_one_bead]
	set oneAtom [atomselect $molId all]
	$oneAtom set type   $::cgfactory::oxidize::TYPE::OH
	$oneAtom set name   $::cgfactory::oxidize::NAME::OH
	$oneAtom set mass   $::cgfactory::oxidize::MASS::OH
	$oneAtom set charge $::cgfactory::oxidize::CHARGE::OH
	$oneAtom set radius $::cgfactory::oxidize::CO_BOND_LENGTH
	$oneAtom set resid   $resid
	$oneAtom set resname $resname
	$oneAtom set chain   $chainid
	$oneAtom set segname $segname
	$oneAtom delete
	return [list $molId]
}