#====================================================
## ::cgfactory::oxidize::add_cg_hydroxyl
# Goal: add oxygen species to a target
# Author: Yuhang Wang
# Date: 11/20/2014

#====================================================
source [file join [::cgfactory::get_oxidize_dir] "oxidize_make_hydroxyl_bead.tcl"]
source [file join [::cgfactory::get_oxidize_dir] "oxidize_find_oxidation_sites.tcl"]

proc ::cgfactory::oxidize::add_cg_hydroxyl {molId selection_str topBottom {oxy_level 1} } {
	## Role: add coarse-grained hydroxyl group to a CG bead
	#  Inputs:
	#	molId: VMD mol ID
	#	selection_str: VMD atom selection string
	#	topBottom: 1 for adding hydroxyl group to the top; -1 for adding to the bottom
	#
	set molIdList [list $molId] ;#< a list of molIds for molecules to be merged
	set _target_set [atomselect $molId $selection_str]
	set n_atoms_selected [$_target_set num]
	
	## if no atom selected, just return the original molId
	if {$n_atoms_selected == 0} {
		puts "WARNING: zero atom selected for oxidation; no oxidation done."
		return molId
	}
	set raw_atomIds [$_target_set get index]
	
	if {$oxy_level == 1 } {
		## no need to do random picking, if the user want to oxidize the whole atom selection
		set atomIds $raw_atomIds
	} else {
		set atomIds [::cgfactory::oxidize::find_oxidation_sites $raw_atomIds $oxy_level]
	}
	
	puts "======= select: $selection_str ====="
	puts "======= raw atomIds: $raw_atomIds ==== "
	puts "======= atoms to be oxidize: $atomIds ====="
	$_target_set delete
	
	## for each target atom, at a hydroxyl bead
	
	foreach _atomid $atomIds {
		puts "== add -OH to atom $_atomid"
		## get target infomation
		lassign [::cgfactory::oxidize::_get_target_info $molId $_atomid] x y z _resid _resname _chainid _segname
			
		## make new hydroxyl bead
		set beadMolId [::cgfactory::oxidize::make_hydroxyl_bead $_resid $_resname $_chainid $_segname ] 
		
		## set X, Y, Z coordinates for the new bead
		set selection_str "all"
		set new_z [expr $z+$topBottom*$::cgfactory::oxidize::CO_BOND_LENGTH]
		::cgfactory::tools::coordinate::set_xyz $beadMolId $selection_str $x $y $new_z
		
		## add to the list
		lappend molIdList $beadMolId	
	}
	
	## merge hydroxyl beads 
	set newMolId [::cgfactory::tools::molecule::mergemols $molIdList]
	
	## add bonds between target atoms and hydroxyl bead
	::cgfactory::oxidize::_link_to_hydroxyl $newMolId $atomIds
	
	## garbage collect
	#  delete all intermediate hydroxyl beads
	::cgfactory::tools::molecule::delete_mols [lrange $molIdList 1 end]

	return $newMolId
}


## helper functions for ::cgfactory::oxidize::add_cg_hydroxyl

proc ::cgfactory::oxidize::_get_target_info {molId atomid} {
	## PRIVAITE proc
	#  Role: get taget atom information
	#  Inputs:
	#	molId: VMD mol ID
	#	atomid: atom index
	#  Return: a list
	#	{x y z resid resname chain segname}
	#
	set _sel [atomselect $molId "index $atomid"]
	set resid   [$_sel get resid]
	set resname [$_sel get resname]
	set chain   [$_sel get chain]
	set segname [$_sel get segname]
	## note: get {x y z} returns a list of lists {{x1 y1 z1} {x2 y2 z2} {x3 y3 z3} }
	# since we only have one atom selected, we just need the first entry
	# this is why I used [lindex ... 0]
	lassign [lindex [$_sel get {x y z}] 0] x y z
	$_sel delete
	return [list $x $y $z $resid $resname $chain $segname]	
}

proc ::cgfactory::oxidize::_link_to_hydroxyl {molId target_indices} {
	## PRIVATE proc:
	#  Role: add bond between target beads specified by target_indices to a nearby OH bead
	#  Inputs:
	#	molId: molecule ID
	#	target_indices: indices for target atoms to be added
	#
	set tol [expr 0.1]
	set name_OH $::cgfactory::oxidize::NAME::OH
	set cutoff [expr $::cgfactory::oxidize::CO_BOND_LENGTH+$tol]
	foreach _atomid $target_indices {
		puts "=== add bond between graphene bead and OH bead for atom $_atomid"
		set _atom1 [atomselect $molId "index $_atomid"]
		set _atom2 [atomselect $molId "name $name_OH and within $cutoff of (index $_atomid)"]
		if {[$_atom2 num] != 1 } {
			puts "ERROR HINT: more than 1 OH bead (found [$_atom2 num]) found within cutoff $cutoff ;"
			puts "This is ambiguous. I could not add bond between hydroxyl group and the target bead"
			exit
			return
		}
		set _id1 $_atomid
		set _id2 [$_atom2 get index]
		
		set _name1 [$_atom1 get name]
		set _name2 [$_atom2 get name]
		set bondtype "${_name1}-${_name2}"
		set bondorder 1 ;# single bond
		
		::cgfactory::tools::topology::add_bond $molId $_id1 $_id2 $bondtype $bondorder
	}
	return
}