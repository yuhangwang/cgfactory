#====================================================
## ::cgfactory::oxidize:oxidize_namespace
# Role: defining oxidize module namespace
# Author: Yuhang Wang
# Date: 11/20/2014

#====================================================

namespace eval ::cgfactory::oxidize {
	variable CO_BOND_LENGTH [expr 2.0]
}


namespace eval ::cgfactory::oxidize::TYPE {
	# Role: define conventions for atom types, names 
	variable atomTypeDict {
		"OH"  "OHG" 
		"OGB" "OGB"
	}
	variable OH  [dict get $atomTypeDict "OH"]
	variable OGB [dict get $atomTypeDict "OGB"]
}

namespace eval ::cgfactory::oxidize::NAME {
	# Role: define conventions for atom types, names 
	variable atomNameDict {
		"OH"  "OHG"
		"OGB" "OGB"
	} 
	variable OH  [dict get $atomNameDict "OH"]
	variable OGB [dict get $atomNameDict "OGB"]
}

namespace eval ::cgfactory::oxidize::MASS {
	# Role: conventions for atom masses
	variable atomMassDict [list \
	"OH"  [expr $::cgfactory::constants::MASS::O + $::cgfactory::constants::MASS::H] \
	"OGB" [expr $::cgfactory::constants::MASS::O + $::cgfactory::constants::MASS::H] \
	]
	variable OH  [dict get $atomMassDict "OH"]
	variable OGB [dict get $atomMassDict "OGB"]
}

namespace eval ::cgfactory::oxidize::CHARGE {
	# Role: conventions for atom masses
	variable atomChargeDict {
		"OH"  0.0
		"OGB" 0.0
	}
	variable OH  [dict get $atomChargeDict "OH"]
	variable OGB [dict get $atomChargeDict "OGB"]
}