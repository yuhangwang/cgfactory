#====================================================
## ::cgfactory::oxidize::find_oxidation_sites
# Goal: find oxidation sites
# Author: Yuhang Wang
# Date: 11/20/2014

#====================================================

proc ::cgfactory::oxidize::find_oxidation_sites {in_list oxy_level} {
	## Role: return an list atom indices that can be used for oxidation reaction
	#  Inputs:
	#	molId: VMD mol ID
	#	selection_str: atom selection string
	#	oxy_level: percentage of oxidation sites
	#
	set N [llength $in_list]
	set n_oxy [::tcl::mathfunc::int [expr $N*$oxy_level]] ;# number of oxidation sites
	
	## randomly pick $n_oxy non-repeating indices from $idSet
	return [::cgfactory::tools::random::pick_items $in_list $n_oxy]
}
