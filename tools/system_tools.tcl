#====================================================
## ::cgfactory::tools::system_info_tools
# Goal: provide helper tools for getting system information
# Author: Yuhang Wang
# Date: 11/18/2014

#====================================================

proc ::cgfactory::tools::system::measure_min {molId {selection_str "all"}} {
	## Role: measure the min vector of the system
	#  Inputs:
	# 	molId: VMD mol ID
	#	selection_str: atom selection string (default: all)
	set _all [atomselect $molId $selection_str]
	lassign [measure minmax $_all] _min _max
	return $_min
}

proc ::cgfactory::tools::system::measure_max {molId {selection_str "all"}} {
	## Role: measure the max vector of the system
	#  Inputs:
	# 	molId: VMD mol ID
	#	selection_str: atom selection string (default: all)
	set _all [atomselect $molId $selection_str]
	lassign [measure minmax $_all] _min _max
	return $_max
}

proc ::cgfactory::tools::system::measure_minmax {molId {selection_str "all"}} {
	## Role: measure the min and max vector of the system
	#  Inputs:
	# 	molId: VMD mol ID
	#	selection_str: atom selection string (default: all)
	#  Return: {xmin ymin zmin xmax ymax zmax}
	#
	set _all [atomselect $molId $selection_str]
	lassign [measure minmax $_all] _min _max
	lassign $_min xmin ymin zmin
	lassign $_max xmax ymax zmax
	return [list $xmin $ymin $zmin $xmax $ymax $zmax]
}

proc ::cgfactory::tools::system::measure_length {molId {selection_str "all"}} {
	## Role: measure the length of the system along X, Y, and Z
	#  Inputs:
	# 	molId: VMD mol ID
	#	selection_str: atom selection string (default: all)
	#  Return: {Lx Ly Lz}
	#

	lassign [::cgfactory::tools::system::measure_minmax $molId $selection_str]\
		xmin ymin zmin  xmax ymax zmax
	set Lx [expr $xmax - $xmin]
	set Ly [expr $ymax - $ymin]
	set Lz [expr $zmax - $zmin]
	return [list $Lx $Ly $Lz]
}