#====================================================
## ::cgfactory::tools::coordinate::coordinate_tools
# Goal: provide helper tools for manipulating coordinates
# Author: Yuhang Wang
# Date: 11/18/2014

#====================================================

proc ::cgfactory::tools::coordinate::move {molId dx dy dz} {
	## Role: translate the whole molecule along a vector [dx dy dz]
	set _sel [atomselect $molId all]
	$_sel moveby [list $dx $dy $dz]
	$_sel delete
	return	
}


proc ::cgfactory::tools::coordinate::set_xyz {molId selection_str new_x new_y new_z} {
	## Role: change the X, Y, Z coordinates of the target
	#  Inputs
	#	molId: VMD mol ID
	#	selection_str: VMD atom selection string
	#	new_x: new X 
	#	new_y: new Y
	# 	new_z: new Z
	#
	set _sel [atomselect $molId all]
	set n [$_sel num]
	
	$_sel set x $new_x
	$_sel set y $new_y
	$_sel set z $new_z
	$_sel delete
	return
}

proc ::cgfactory::tools::coordinate::recenter {molId} {
	## Role: recenter the system to be around the origin
	#  Inputs:
	#	molId: VMD mol ID
	#
	set _sel [atomselect $molId all]
	$_sel moveby [vecinvert [measure center $_sel]]
	return
}

proc ::cgfactory::tools::coordinate::get_coords1D {molId atomIds axis} {
	## Role: return the coordinates of atoms only along a chosen axis
	#  Inputs: 
	#	molId: VMD mol ID
	#	atomIds: atom indices
	#	axis: the chosen axis
	#
	set _selection_str "index "
	
	foreach _atomid $atomIds {
		append _selection_str " $_atomid"
	}
	
	set _sel [atomselect $molId $_selection_str]
	set coords [$_sel get $axis]
	$_sel delete
	
	return $coords
}

proc ::cgfactory::tools::coordinate::get_coords {molId atomIds} {
	## Role: return the coordinates of atoms
	#  Inputs: 
	#	molId: VMD mol ID
	#	atomIds: atom indices
	#	axis: the chosen axis
	#
	set _selection_str "index "
	
	foreach _atomid $atomIds {
		append _selection_str " $_atomid"
	}
	set _sel [atomselect $molId $_selection_str]
	set coords [$_sel get {x y z}] 
	$_sel delete
	
	return $coords
}
