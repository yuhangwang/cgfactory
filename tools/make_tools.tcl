#====================================================
## ::cgfactory::tools::make_beads_tools
# Goal: make new beads from scratch
# Author: Yuhang Wang
# Date: 11/19/2014

#====================================================

proc ::cgfactory::tools::make::make_one_bead {} {
	set xyz_file [::cgfactory::get_one_atom_file]
	set molId [mol new $xyz_file autobonds no waitfor all]
	return $molId
}