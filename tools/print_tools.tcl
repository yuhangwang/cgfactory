#====================================================
## ::cgfactory::tools::print
# Goal: provide helper tools for printing
# Author: Yuhang Wang
# Date: 11/18/2014

#====================================================

proc ::cgfactory::tools::print::array2D {name_array2D n_rows n_cols} {
	## Role: print out a 2D Tcl array
	#  Inputs
	#	array2D: a 2D array
	#	n_rows: number of rows
	#	n_cols: number of columns
	#
	puts "============================================"
	upvar $name_array2D ptr_array2D
	for {set i 0} {$i < $n_rows} {incr i} {
		for {set j 0} {$j < $n_cols} {incr j} {
			puts -nonewline "$ptr_array2D($i,$j)\t"
		}
		puts ""
	}
	puts "============================================"
	return
}