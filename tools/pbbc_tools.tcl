#====================================================
## ::cgfactory::tools::pbbc_tools
# Goal: set up Periodic Bonded Boundary Condition (PBBC)
# Author: Yuhang Wang
# Date: 11/20/2014

#====================================================


proc ::cgfactory::tools::pbbc::ez_add_pbbc {molId  pbbc_axes target_object_selection_str half_width bond_length_cutoff bond_type bond_search_tol angle_type} {
	## An easy way of adding PBBC for an object 
	#  INPUTS:
	#  molId: VMD molecule ID
	#  pbbc_axes: a list like {'x' 'y'}
	#  target_ojbect_selection_str: an atom selection string for the selecting the target object
	#  half_width: half of the selection width at one edge of the target object
	#  bond_length_cutoff: distance cutoff within which two atoms will be considered bonded
	#  bond_type: bond type for the new bonds to be added
	#  bond_search_tol: tolerance when searching new bonds with distance cutoff
	#
	lassign [::cgfactory::tools::pbc::get_object_minmax $molId $target_object_selection_str] objectMinList objectMaxList
	lassign [::cgfactory::tools::pbc::get_box_size $molId] boxLx boxLy boxLz
	puts "boxLx: $boxLx; boxLy: $boxLy; boxLz: $boxLz"
	set boxLengthList [list $boxLx $boxLy $boxLz]
	set num_pbbc_dims [llength $pbbc_axes]
	set edge_bead_atomIds {} ;# list IDs for the atom at the edge of the graphene
	
	# find edge beads and add new bonds
	for {set i 0} {$i < $num_pbbc_dims} {incr i} {
		set pbc_axis [lindex $pbbc_axes $i]
		set min_position [lindex $objectMinList $i]
		set max_position [lindex $objectMaxList $i]
		
		puts "pbbc axis: $pbc_axis"
		set list_atomIds {}
		foreach selection_center [list $min_position $max_position] {
			set sel_str [::cgfactory::tools::select::band_str $pbc_axis $selection_center $half_width $target_object_selection_str]
			set sel [atomselect $molId $sel_str]
			set atomIds [$sel get index]
			lappend list_atomIds $atomIds
        		foreach id $atomIds {lappend edge_bead_atomIds $id}
		}
		lassign $list_atomIds atomIds_target atomIds_neighbor
		
		::cgfactory::tools::pbbc::setup_pbbc_bonds $molId $atomIds_target $atomIds_neighbor $boxLengthList  $bond_length_cutoff $bond_type $bond_search_tol
	}
	
	# add new angles
	puts "== Add PBBC angles"
	set forbidden_angle_degrees $::cgfactory::graphene::BAD_ANGLE_DEGREES
	set edge_bead_atomIds [lsort -unique $edge_bead_atomIds]
	::cgfactory::tools::pbbc::setup_pbbc_angles $molId $edge_bead_atomIds $forbidden_angle_degrees  $angle_type
	
	# add new dihedrals
#	puts "== Add PBBC dihedrals for graphene"
#	::cgfactory::tools::pbbc::setup_graphene_dihedrals $molId $target_object_selection_str $dihedral_type
#	
	return
}


proc ::cgfactory::tools::pbbc::setup_graphene_dihedrals {molId selection_str dihedral_type} {
	## Add PBBC Dihedrals for graphene
	#  Inputs:
	#	molId: VMD molecule ID
	#	selection_str: an atom selection string for the target
	#	dihedral_type: name of the dihedral type to be created
	#  Developer's note: the key idea is to use graphene residues to handle the complex index problem
	#  Each graphene residue is a row of grpahene beads
	#
	set new_dihedral_list {}
	set list_of_atomId_lists {}
	set left_edge_atomIds {}
	set right_edge_atomIds {}
	set sel [atomselect $molId $selection_str ]
	set list_resids [lsort -unique [$sel get resid]]
	set total_num_graphene_residues [llength $list_resids]
	$sel delete
	
	foreach _resid $list_resids {
		set _sel [atomselect $molId "($selection_str) and (resid $_resid)"]
		set _atomIds [$_sel get index]
		puts "residue $_resid 's atoms: $_atomIds"
		lappend list_of_atomId_lists $_atomIds
		lappend left_edge_atomIds [lindex $_atomIds 0]
		lappend right_edge_atomIds [lindex $_atomIds end]
	}
	set bottom_edge_atomIds [lindex $list_of_atomId_lists 0]
	set top_edge_atomIds [lindex $list_of_atomId_lists end]

	# (1). Add PBBC dihedrals between top/bottom edges
	set _new_dihedrals [::cgfactory::tools::pbbc::add_graphene_pbbc_dihedral_top_bottom \
	 				$top_edge_atomIds $bottom_edge_atomIds $dihedral_type]
	foreach _dihedral $_new_dihedrals {lappend new_dihedral_list $_dihedral}
	
	
	# (2). Add type-1 dihedrals between left/right edges
	set _new_dihedrals [::cgfactory::tools::pbbc::add_graphene_pbbc_dihedral_left_right_type1 \
	 			$left_edge_atomIds $right_edge_atomIds $dihedral_type]
	foreach _dihedral $_new_dihedrals {lappend new_dihedral_list $_dihedral}
	
	
	# (3). Add type-2 dihedrals between left/right edges
	set _new_dihedrals [::cgfactory::tools::pbbc::add_graphene_pbbc_dihedral_left_right_type2 \
	 				$left_edge_atomIds $right_edge_atomIds $list_of_atomId_lists $dihedral_type]
	foreach _dihedral $_new_dihedrals {lappend new_dihedral_list $_dihedral}
	
	# (4). Add PBBC hinge dihedrals
#	if {$total_num_graphene_residues >= 3} {
#		set _new_dihedrals [::cgfactory::tools::pbbc::add_graphene_pbbc_hinge_dihedrals \
#		 				$list_of_atomId_lists $dihedral_type]
#        	foreach _dihedral $_new_dihedrals {lappend new_dihedral_list $_dihedral}
#	}
#	
	# (5). merge old/new dihedral list
	set old_dihedrals [::cgfactory::tools::topology::get_dihedral_list $molId ]
	
	set updated_full_dihedrals [concat $old_dihedrals $new_dihedral_list]
	
	::cgfactory::tools::topology::update_dihedral_list $molId $updated_full_dihedrals
	return
}


proc ::cgfactory::tools::pbbc::add_graphene_pbbc_dihedral_top_bottom {top_edge_atomIds bottom_edge_atomIds dihedral_type} {
	## Add PBBC dihedrals between top and bottom graphene residues
	#  INPUTS:
	#	top_edge_atomIds: list of atom IDs for the beads on the top edge, e.g. {9 10 11}
	#	bottom_edge_atomIds: list of atom IDs for the beads on the bottom edge, e.g. {0 1 2}
	#	dihedral_type: type of the dihedral to be added, e.g. "CGC-CGC-CGC-CGC"
	#
	set output_dihedral_list {}
	#  add the 1st type of dihedrals
	set num_to_be_wrapped 1 ;# number of items to be wrapped from the end of the list
	set atomIds1 [::cgfactory::tools::list::wrap_list $top_edge_atomIds $num_to_be_wrapped] ;# e.g. {11 9 10}
	set atomIds1 [concat [lindex $atomIds1 end] $atomIds1];# e.g. {10 11 9 10}
	set atomIds2 [concat [lindex $bottom_edge_atomIds end] $bottom_edge_atomIds ] ;# e.g. {2 0 1 2}
	set _new_dihedrals [::cgfactory::tools::topology::create_dihedrals $dihedral_type $atomIds1 $atomIds2]
	puts "Type-1: new top/bottom PBBC dihedrals: $_new_dihedrals"
	foreach _dihedral $_new_dihedrals {lappend output_dihedral_list $_dihedral}
	
	# add 2nd type of dihedrals
	set atomIds1 [lrange $atomIds2 0 end] ;# e.g. {2 0 1 2}
	set atomIds2 [concat [lindex $top_edge_atomIds end] $top_edge_atomIds ] ;# e.g. {11 9 10 11}
	set _new_dihedrals [::cgfactory::tools::topology::create_dihedrals $dihedral_type $atomIds1 $atomIds2]
	puts "Type-2: new top/bottom PBBC dihedrals: $_new_dihedrals"
	foreach _dihedral $_new_dihedrals {lappend output_dihedral_list $_dihedral}
	return $output_dihedral_list
}

proc ::cgfactory::tools::pbbc::add_graphene_pbbc_dihedral_left_right_type1 {left_edge_atomIds right_edge_atomIds dihedral_type} {
	## Add type-1 PBBC dihedrals between beads from left and right edge of the graphene
	#  INPUTS:
	#	left_edge_atomIds: list of atom IDs for beads on the left edge of the graphene
	#	right_edge_atomIds: list of atom IDs for beads on the right edge of the graphene
	#	dihedral_type: string: type of the new dihedral
	#
	
	set output_dihedral_list {}
	for {set i 0 } {$i < [expr [llength $left_edge_atomIds]-1] } {incr i} {
		set atomIds1 {}
		set atomIds2 {}
		set id1 [lindex $right_edge_atomIds $i]
		set id2 [lindex $left_edge_atomIds $i]
		set id3 [lindex $right_edge_atomIds [expr $i+1] ]
		set id4 [lindex $left_edge_atomIds [expr $i+1] ]
		if {[::cgfactory::math::basics::isEven $i]} {
			lappend atomIds1 $id1 $id2
			lappend atomIds2 $id3 $id4
		} else {
			lappend atomIds1 $id3 $id4
			lappend atomIds2 $id1 $id2
		}
		set _new_dihedrals [::cgfactory::tools::topology::create_dihedrals $dihedral_type $atomIds1 $atomIds2]
		puts "Type-1: new left/right dihedrals: $_new_dihedrals"
		foreach _dihedral $_new_dihedrals {lappend output_dihedral_list $_dihedral}
	}
	return $output_dihedral_list
}


proc ::cgfactory::tools::pbbc::add_graphene_pbbc_dihedral_left_right_type2 {left_edge_atomIds right_edge_atomIds list_of_atomId_lists dihedral_type} {
	## Add type-2 PBBC dihedrals between beads from left and right edge of the graphene
	#  INPUTS:
	#	left_edge_atomIds: list of atom IDs for beads on the left edge of the graphene
	#	right_edge_atomIds: list of atom IDs for beads on the right edge of the graphene
	#	dihedral_type (string): type of the new dihedral
	#
	set output_dihedral_list {}
	for {set i 0 } {$i < [expr [llength $list_of_atomId_lists] - 1]} {incr i} {
		if {[::cgfactory::math::basics::isEven $i]} {
			set atomIdList1 [lindex $list_of_atomId_lists [expr $i+1] ]
			set atomIdList2 [lindex $list_of_atomId_lists $i]
		} else {
			set atomIdList1 [lindex $list_of_atomId_lists $i]
			set atomIdList2 [lindex $list_of_atomId_lists [expr $i+1] ]
		}
		set len1 [llength $atomIdList1]
		set len2 [llength $atomIdList2]
		set part1 [lindex $atomIdList1 [expr $len1 - 2] ]
		set part2 [lindex $atomIdList1 end]
		set part3 [lindex $atomIdList1 0]
		set atomIds1 [concat $part1 $part2 $part3]
		set part1 [lindex $atomIdList2 end]
		set part2 [lrange $atomIdList2 0 1]
		set atomIds2 [concat $part1 $part2]
		
		set _new_dihedrals [::cgfactory::tools::topology::create_dihedrals $dihedral_type $atomIds1 $atomIds2]
		puts "Type-2: new left/right dihedrals: $_new_dihedrals"
		foreach _dihedral $_new_dihedrals {lappend output_dihedral_list $_dihedral}
	}
	return $output_dihedral_list
}

proc ::cgfactory::tools::pbbc::add_graphene_pbbc_hinge_dihedrals {list_of_atomId_lists dihedral_type} {
	## Add graphene PBBC hinge dihedrals (if there are at least three graphene residues
	#  INPUTS:
	#	list_of_atomId_lists: list of lists of atom IDs 
	#	dihedral_type (string): type of the new dihedral
	#
	
	set n_residues [llength $list_of_atomId_lists]
	set output_dihedral_list {}
	# (1). add the hinge dihdrals between top and bottom edges (along X)
	set i_list1 [list [expr $n_residues - 2] [expr $n_residues - 1] 0 ]
	set i_list2 [list [expr $n_residues - 1] 0 1 ]
	foreach i_list [list $i_list1 $i_list2] {
		lassign $i_list i1 i2 i3
		set list1 [lindex $list_of_atomId_lists $i1]
		set list2 [lindex $list_of_atomId_lists $i2]
		set list3 [lindex $list_of_atomId_lists $i3]
		set list1 [concat [lindex $list1 end] $list1 ]
		set list2 [concat [lindex $list2 end] $list2 ]
		set list3 [concat [lindex $list3 end] $list3 ]
		set _new_dihedrals [::cgfactory::tools::topology::create_graphene_hinge_dihedrals $dihedral_type $list1 $list2 $list3]
		puts "Type1: hinge dihedrals: $_new_dihedrals"
		foreach _dihedral $_new_dihedrals {lappend output_dihedral_list $_dihedral}
		
	}
	
	
	# (2). add missing hinge dihedrals for residues in between top and bottom residues
	set middle_ids [::cgfactory::tools::list::range 1 [expr $n_residues - 1] ]
	foreach i $middle_ids {
		set list1 [lindex $list_of_atomId_lists [expr $i - 1]]
		set list2 [lindex $list_of_atomId_lists [expr $i]]
		set list3 [lindex $list_of_atomId_lists [expr $i + 1]]
		set list1 [list [lindex $list1 end] [lindex $list1 0] ]
		set list2 [list [lindex $list2 end] [lindex $list2 0] ]
		set list3 [list [lindex $list3 end] [lindex $list3 0] ]
		set _new_dihedrals [::cgfactory::tools::topology::create_graphene_hinge_dihedrals $dihedral_type $list1 $list2 $list3]
		puts "Type2: hinge dihedrals: $_new_dihedrals"
		foreach _dihedral $_new_dihedrals {lappend output_dihedral_list $_dihedral}
	}
	return $output_dihedral_list
	
}

proc ::cgfactory::tools::pbbc::setup_pbbc_bonds {molId atomIds_target atomIds_neighbor boxL bond_length_cutoff bond_type {tol 0.2} } {
	## Role: set up PBBC condition (i.e. add bonds across boundaries)
	#  INPUTS:
	#	molId: VMD mol ID
	#	atomIds_target: the atom indices for the target beads
	#	atomIds_neighbor: the atom indices for potential neighbors
	#	boxL: a list of box sizes along each dimension (list of 3 numbers)
	#	bond_length_cutoff: cutoff distance for adding new bonds
	#	bond_type: type of the bond
	#	tol: tolerance for bond_length_cutoff
	#
	
	set L1 [llength $atomIds_target]
	set L2 [llength $atomIds_neighbor]
	if {$L1 == 0 || $L2 == 0 } {
		puts "ERROR HINT: the atom index list \"atomIds_target\" and \"atomIds_neighbor\"  should not be empty"
		exit
	}
	
	## get coordinates
	set coords_target   [::cgfactory::tools::coordinate::get_coords $molId $atomIds_target   ]
	set coords_neighbor [::cgfactory::tools::coordinate::get_coords $molId $atomIds_neighbor ]

	## calculate pairwise-distances
	set usePBC 1 ;# apply PBC condition
	array set pairwise_dist_matrix [::cgfactory::tools::distance::pairwise_distances $coords_target $coords_neighbor $usePBC $boxL]

	## add pbbc bonds
	::cgfactory::tools::pbbc::add_new_bonds $molId $bond_length_cutoff $tol \
		"pairwise_dist_matrix" $atomIds_target $atomIds_neighbor $bond_type
	
	return
		
}




proc ::cgfactory::tools::pbbc::setup_pbbc_angles {molId atomIds forbidden_angle_degrees new_angle_type} {
	## add new angles across the boundaries.
	#  INPUTS:
	#	molId: VMD molecule ID
	#	atomIds: list of atom indices
	#	forbidden_angle_degress: any angle of the degree from this list will not be added
	#  Strategy: 
	#	For each atom in the list atomIds, find all bonds connected to it.
	#	use this bonds to add new angles
	#
	set all_new_angles {}
	puts "== setup_pbbc_angles: atomIds = $atomIds"
	foreach _atomId $atomIds {
		puts "atomId: $_atomId"
		set neighborAtomIds [::cgfactory::tools::topology::get_bonded_partner_indices $molId $_atomId ]
		set sel_str "index $_atomId or index [join $neighborAtomIds]"
		set old_angle_lists [::cgfactory::tools::topology::get_angle_list $molId $sel_str ]
		set new_angles [::cgfactory::tools::pbbc::find_new_angles $molId $_atomId $neighborAtomIds $old_angle_lists $forbidden_angle_degrees $new_angle_type]
		foreach _item $new_angles {
			puts "== add new angle: $_item"
			lappend all_new_angles $_item 
		}
	}
	::cgfactory::tools::topology::batch_add_new_angles $molId $all_new_angles
	return
}


proc ::cgfactory::tools::pbbc::find_new_angles {molId targetAtomId neighborAtomIds old_angle_list forbidden_angle_degrees new_angle_type} {
	## find new angles for a given atom
	#  INPUTS:
	#	molId: VMD molecule ID
	#	targetAtomId: index of the target atom
	#	neighborAtomIds: indices for the neighboring atoms of the target atom
	#	old_angle_list: list of existing angles centered at the target atom
	#	forbidden_angle_degrees: list of forbidden angle degrees 
	#	new_angle_type: type of the new angle to be added
	#
	#  RETURN: a list of new angles of the format {{type id1 id2 id3} {type id1 id2 id3}}
	#
	set num_neighbors [llength $neighborAtomIds]
	set output_angle_list {}
	for {set i 0} {$i < $num_neighbors} {incr i} {
		for {set j [expr $i+1]} {$j < $num_neighbors} {incr j} {
			set id1 [lindex $neighborAtomIds $i]
			set id2 [lindex $neighborAtomIds $j]
			if { $id1 == $id2 } {continue}
			set _new_angle [list $new_angle_type $id1 $targetAtomId $id2]
			
			# 1). check whether this angle is new
			set isNewAngle [::cgfactory::tools::pbbc::checkWhetherNewAngle $_new_angle $old_angle_list]
			if {!$isNewAngle} { continue }
			
			# 2) check whether the magnitude of this angle is allowed
			lassign [::cgfactory::tools::pbc::get_wrapped_coordinates $molId $targetAtomId $id1] coordMid coord1
			lassign [::cgfactory::tools::pbc::get_wrapped_coordinates $molId $targetAtomId $id2] _ coord2
			set _radian  [::cgfactory::tools::measure::angle $coord1 $coordMid $coord2]
			set _degree [::cgfactory::math::unit::radian2degree $_radian]
			set _degree [expr round($_degree)]
			set isAngleAllowed [::cgfactory::tools::pbbc::checkWetherAngleAllowed $_degree $forbidden_angle_degrees]
			if {!$isAngleAllowed} { continue }
			
			## add degree values to the type name
			set type_with_degree "${new_angle_type}-${_degree}"
			set _new_angle [lreplace $_new_angle 0 0 $type_with_degree]
			lappend output_angle_list $_new_angle
		}
	}
	return $output_angle_list
}

proc ::cgfactory::tools::pbbc::checkWhetherNewAngle {suspected_new_angle old_angle_list} {
	## Check whether a suspected angle is a new angle 
	#	i.e. not belongs to old_angle_list
	#  INPUTS:
	#	suspected_new_angle: a list of the form {type id1 id2 id3}
	#	old_angle_list: list of lists of angles, e.g. {{type 1 2 3} {type 4 5 6}}
	#  RETURN: 1 or 0
	#
	lassign $suspected_new_angle _ new_id1 new_id2 new_id3
	foreach _angle $old_angle_list {
		lassign $_angle _ old_id1 old_id2 old_id3
		set match1 [expr ($new_id1 == $old_id1) && ($new_id2 == $old_id2) && ($new_id3 == $old_id3)]
		set match2 [expr ($new_id1 == $old_id3) && ($new_id2 == $old_id2) && ($new_id3 == $old_id1)]
		if {[expr $match1 || $match2]} { return 0 }
	}
	return 1
}

proc ::cgfactory::tools::pbbc::checkWetherAngleAllowed {angle forbidden_angles} {
	## Check whether an angle is allowed (i.e. not in the forbidden_angles list)
	#  INPUTS:
	#	angle: suspected angle (in degrees)
	#	forbidden_angles: a list angles (in degrees)
	#  RETURN: 1 or 0
	#
	set result [lsearch $forbidden_angles $angle] 
	return [expr $result == -1]
}


proc ::cgfactory::tools::pbbc::add_new_bonds {molId bond_length_cutoff tol ptr_pairwise_dist_array atomIds_row atomIds_col bond_type} {
	## Role: add pbc bonds between beads that are close
	#  INPUTS:
	#	molId: VMD mol ID
	#	bond_length_cutoff: within this cutoff+tol, two atoms will be bonded together
	#	tol: tolerance for the bond_length_cutoff
	#	ptr_pairwise_dist_array: name of the variable of the 2D array for pairwise distances
	#	atomIds_row: atom indices corresponding to row indices
	#	atomIds_col: cf. atomIds_col
	#  RETURN: a list of new bonds added
	
	set new_bond_order [expr 1.0]
	upvar $ptr_pairwise_dist_array pairdistArray2D
	set N_rows [llength $atomIds_row]
	set N_cols [llength $atomIds_col]
	set threshold [expr $bond_length_cutoff+$tol]
	set list_bonds_to_add {}
	for {set i 0} {$i < $N_rows} {incr i} {
		for {set j 0} {$j < $N_cols} {incr j} {
			set d $pairdistArray2D($i,$j)
			if {$d<$threshold} {
				set id1 [lindex $atomIds_row $i]
				set id2 [lindex $atomIds_col $j]
				set partners_of_atom1 [::cgfactory::tools::topology::get_bonded_partner_indices $molId $id1]
				# only add new bond id1-id2 when it is not in the old bond lists
				if {[lsearch $partners_of_atom1 $id2] == -1 } { 
					if {$id1 < $id2} {
						set new_bond [list $id1 $id2 $bond_type $new_bond_order]
					} else {
						set new_bond [list $id2 $id1 $bond_type $new_bond_order]
					}
					lappend list_bonds_to_add $new_bond
				}
			}
		}
	}
	
	::cgfactory::tools::topology::batch_add_new_bonds $molId $list_bonds_to_add
	puts "Done with add new bonds!"
	return 
	
}




	