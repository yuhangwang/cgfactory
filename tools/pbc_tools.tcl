## ::cgfactory::tools::pbc_box_tools
# Goal: provide helper tools for manipulating pbc box
# Author: Yuhang Wang
# Date: 11/18/2014

#====================================================

proc ::cgfactory::tools::pbc::set_box_size {molId Lx Ly Lz} {
	## Role: define the size of the unit box
	pbc set [list $Lx $Ly $Lz] -molid $molId
}

proc ::cgfactory::tools::pbc::get_box_size {molId} {
	## Role: get the length information of the box
	#  Return: a list of lengths along X, Y, Z
	return [molinfo $molId get {a b c}]
}

proc ::cgfactory::tools::pbc::get_box_center {molId} {
	## Get the center of the PBC box
	#  RETURN: a list {x y z} 
	return [lindex [molinfo $molId get center] 0]
}

proc ::cgfactory::tools::pbc::get_box_minmax_unpacked {molId} {
	## Get the min/max of the PBC box in an unpacked format
	#  RETURN: a list: {xmin ymin zmin xmax ymax zmax}
	lassign [::cgfactory::tools::pbc::get_box_size $molId] Lx Ly Lz
	lassign [::cgfactory::tools::pbc::get_box_center $molId] xcen ycen zcen
	puts "Lx $Lx"
	puts "Ly $Ly"
	puts "Lz $Lz"
	puts "xcen $xcen"
	puts "ycen $ycen"
	puts "zcen $zcen"
	set xmin [expr $xcen - $Lx/2.0]
	set ymin [expr $ycen - $Ly/2.0]
	set zmin [expr $zcen - $Lz/2.0]
	set xmax [expr $xcen + $Lx/2.0]
	set ymax [expr $ycen + $Ly/2.0]
	set zmax [expr $zcen + $Lz/2.0]
	return [list $xmin $ymin $zmin $xmax $ymax $zmax]
}

proc ::cgfactory::tools::pbc::get_box_minmax {molId} {
	## Get the min/max of the PBC box
	#  RETURN: a list: {{xmin ymin zmin} {xmax ymax zmax}}
	lassign [::cgfactory::tools::pbc::get_box_minmax_unpacked $molId] xmin ymin zmin xmax ymax zmax
	return [list [list $xmin $ymin $zmin] [list $xmax $ymax $zmax] ]
}


proc ::cgfactory::tools::pbc::get_object_minmax {molId selection_str} {
	## Get the min/max of a selected object
	#  Inputs:
	#	molId: VMD molecule ID
	#	selection_str: atom selection string
	#  Outputs:
	#	list: {{xmin ymin zmin} {xmax ymax zmax}}
	set sel [atomselect $molId $selection_str]
	set minmax_list [measure minmax $sel]
	$sel delete
	return $minmax_list
}

proc ::cgfactory::tools::pbc::get_object_minmax_unpacked {molId selection_str} {
	## Get the min/max of a selected ojbect in an unpacked format
	#  Inputs:
	#	molId: VMD molecule ID
	#	selection_str: atom selection string
	#  Outputs:
	#	list: {xmin ymin zmin xmax ymax zmax}
	lassign [::cgfactory::tools::pbc::get_object_minmax $molId $selection_str] Min Max
	lassign $Min xmin ymin zmin
	lassign $Max xmax ymax zmax
	return [list $xmin $ymin $zmin $xmax $ymax $zmax]
	
}

proc ::cgfactory::tools::pbc::smart_set_box_size {molId {x_margin 0} {y_margin 0} {z_margin 0} } {
	## Role: set up proper PBC box sizes
	#  Implementation: add x_margin to X dimension, and so on for Y, Z dimensions
	#  Inputs: 
	#	molId: VMD mol ID
	#	x_margin: margin that will be added to the length of the box along X (default:0)
	#	y_margin: cf. x_margin (default: 0)
	#	z_margin: cf. y_margin (default: 0)
	
	## define PBC box sizes
	set selection_str "all"
	lassign [::cgfactory::tools::system::measure_length $molId $selection_str] Lx Ly Lz
	set Lx [expr $Lx + $x_margin]
	set Ly [expr $Ly + $y_margin]
	set Lz [expr $Lz + $z_margin]
	
	::cgfactory::tools::pbc set_box_size $molId $Lx $Ly $Lz
	## return new box information
	return [::cgfactory::tools::pbc::get_box_size $molId]
}

proc ::cgfactory::tools::pbc::min_image_distance {dist boxL} {
	## Role: return the minimum image distance under periodic boundary condition in 1D
	#  Inputs:
	#	dist: distance
	#	boxL: length of the PBC box along the chosen dimension
	set halfBoxL [expr 0.5*$boxL]
	set factor [expr floor($dist/$halfBoxL)]
	return [expr $boxL*$factor - $dist]
}

proc ::cgfactory::tools::pbc::get_wrapped_coordinates {molId refAtomId targetAtomId } {
	## Role: return a list of two coordinates that has been PBC wrapped
	#	such that the two atoms follow the minimum image convention.
	#  Inputs:
	#	molId: VMD mol ID
	#	refAtomId: index for reference atom whose coordinates will be preserved
	#	targetAtomId: index for target atom whose coordinates will be wrapped
	#
	set _atom1  [atomselect $molId "index $refAtomId "]
	set _atom2  [atomselect $molId "index $targetAtomId "]
	set _coord1 [lindex [$_atom1 get {x y z}] 0]
	set _coord2 [lindex [$_atom2 get {x y z}] 0]
	set boxL    [::cgfactory::tools::pbc::get_box_size $molId]
	set n_dimension 3
	
	set output [list $_coord1]
	set wrapped_coord2 {}
	
	## Wrap each dimension of _coord2
	for {set dim 0 } {$dim < $n_dimension } {incr dim} {
		set c1 [lindex $_coord1 $dim]
		set c2 [lindex $_coord2 $dim]
		set L  [lindex $boxL $dim]
		set halfL [expr 0.5*$L]
		set d_raw [expr $c1 - $c2] ;# 1D distance (with sign)
		set sgn [::cgfactory::math::basics::sign $d_raw] ;# sign of $d
		set d_abs [expr abs($d_raw)]
		set new_c2 [expr $c2 + $sgn*floor($d_abs/$halfL)*$L]
		lappend wrapped_coord2 $new_c2
	}
	
	lappend output $wrapped_coord2
	return $output
}