#====================================================
## ::cgfactory::tools::distance_tools
# Goal: provide tools for calculating distance related quantities
# Author: Yuhang Wang
# Date: 11/23/2014

#====================================================


proc ::cgfactory::tools::distance::measure_distance {vector1 vector2 {usePBC 0} {boxLengthList 0} } {
	## Role: measure distance between two vectors
	#  Inputs:
	#	vector1: 1st vector
	#	vector2: 2nd vector
	#	usePBC: use PBC minimum image convention? (default: false)
	#	boxLengthList: if usePBC is true, this should be set to a list of PBC box lengths along each dimension
	#
	lassign $vector1 x1 y1 z1
	lassign $vector2 x2 y2 z2
	set n_dimensions 3
	set dist 0
	if {$usePBC} {
		if { $boxLengthList == 0 } {puts "ERROR HINT: you must specify the PBC box sizes when usePBC is set to 1"; exit;}
		if {[llength $boxLengthList] != 3} {
			puts "ERROR HINT: you must specify the PBC box sizes to be a list of 3 numbers \
			when usePBC is set to 1"; 
			exit;
		}
	}
	
	for {set i 0} {$i < $n_dimensions} {incr i} {
		set c1 [lindex $vector1 $i]
		set c2 [lindex $vector2 $i]
		set d [::tcl::mathfunc::abs [expr $c1 - $c2]]
		if {$usePBC} {
			set L [lindex $boxLengthList $i] ;#< box length along the current dimension
			set d [::cgfactory::tools::pbc::min_image_distance $d $L]
		}
		set dist [expr $dist + $d*$d]
	}
	return [expr sqrt($dist)]
}

proc ::cgfactory::tools::distance::pairwise_distances {list_of_vectors1 list_of_vectors2 {usePBC 0} {boxLengthList 0} } {
	## Role: compute the pairwise distance matrix
	#  Inputs:
	#	list_of_vectors1:
	#	list_of_vectors2:
	#	usePBC: use PBC minimum image convention? (default: false)
	#	boxLengthList: if usePBC is true, this should be set to a list of PBC box lengths along each dimension
	#
	set L1 [llength $list_of_vectors1]
	set L2 [llength $list_of_vectors2]
	
	## build pairdist_matrix
	array set pairdist_matrix {}
	for {set i 0} {$i < $L1} {incr i} {
		for {set j 0} {$j < $L2} {incr j} {
			set v1 [lindex $list_of_vectors1 $i]
			set v2 [lindex $list_of_vectors2 $j]
			set d  [::cgfactory::tools::distance::measure_distance $v1 $v2 $usePBC $boxLengthList]
			set pairdist_matrix($i,$j) $d
		}
	}

	return [array get pairdist_matrix]
	
}

