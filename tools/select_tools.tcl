#====================================================
## ::cgfactory::tools::select_tools
# Goal: provide convient tools for make atom selections
# Author: Yuhang Wang
# Date: 11/20/2014

#====================================================

proc ::cgfactory::tools::select::band_str {axis center  {half_band_width 0.2} {base_selection_str  "all"}} {
	## Role: select beads within center+/-half_band_width along axis
	#	return an atom selection string
	#  Inputs:
	#	molId: VMD mol ID
	#	axis: e.g. 'x' or 'y' or 'z'
	#	center: center of the selection
	#	half_band_width: half width of the selection region (default: 0.2)
	#	base_selection_str: atom selection string (default: "all")
	#
	set upper [expr $center + $half_band_width]
	set lower [expr $center - $half_band_width]
	return "$base_selection_str and ($axis > $lower and $axis < $upper)"
}


proc ::cgfactory::tools::select::around_xyz {molId base_selection_str cutoff cenX cenY cenZ} {
	## Role: return indices for attoms within $cutoff from a point [cenX cenY cenZ]
	#  Inputs:
	#	molId: VMD mol ID
	#	base_selection_str: base selection string
	#	cutoff: cutoff distance
	#	cenX: x for the center
	#	cenY: y for the center
	#	cenZ: z for the center
	#
	set sel_str "$base_selection_str "
	append sel_str "and sqrt(sqr(x-$cenX)+sqr(y-$cenY)+sqr(z-$cenZ)) < $cutoff "
	set _sel [atomselect $molId $sel_str]
	return [$_sel get index]
}