## functions for measurement

proc ::cgfactory::tools::measure::angle {coord1 coord2 coord3} {
	## measure the angle using the coordinates of 3 points
	set vec1 [vecsub $coord1 $coord2]
	set vec2 [vecsub $coord3 $coord2]
	set dot [vecdot $vec1 $vec2]
	set len1 [veclength $vec1]
	set len2 [veclength $vec2]
	set cos_val [expr $dot/($len1*$len2)]
	set angle [expr acos($cos_val)]
}

