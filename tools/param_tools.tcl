#====================================================
## ::cgfactory::tools::bead_param_tools
# Goal: provide helper tools for manipulating paramters
#	of atoms
# Author: Yuhang Wang
# Date: 11/18/2014

#====================================================

proc ::cgfactory::tools::param::set_residue_param {molId selection_str new_resid new_resname new_segname new_chainId} {
	## set the parmeters for an atom slection object
	set selObj [atomselect $molId $selection_str]
	$selObj set resid $new_resid
	$selObj set resname $new_resname
	$selObj set segname $new_segname
	$selObj set chain $new_chainId
	$selObj delete
	return
}
