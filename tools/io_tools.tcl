#====================================================
## ::cgfactory::tools::io_tools
# Goal: provide helper tools for data IO
# Author: Yuhang Wang
# Date: 11/18/2014

#====================================================

proc ::cgfactory::tools::io::save_pdb {output_dir output_prefix molId {sel_txt "all"} } {
	## Save molecule into a pdb file
	set target [atomselect $molId $sel_txt]
	animate write pdb [file join ${output_dir} ${output_prefix}.pdb] sel $target $molId 
	$target delete
}

proc ::cgfactory::tools::io::save_psf {output_dir output_prefix molId {sel_txt "all"} } {
	## Save molecule into a psf file
	set target [atomselect $molId $sel_txt]
	animate write psf [file join ${output_dir} ${output_prefix}.psf] sel $target $molId 
	$target delete
}

proc ::cgfactory::tools::io::save_xml {output_dir output_prefix molId {sel_txt "all"} } {
	## Save molecule into a psf file
	set target [atomselect $molId $sel_txt]
	animate write hoomd [file join ${output_dir} ${output_prefix}.xml] sel $target $molId 
	$target delete
}

proc ::cgfactory::tools::io::save_lmpd {output_dir output_prefix molId {sel_txt "all"} {format molecular}} {
	## Save molecule into a psf file
	#  Inputs:
	#	output_dir: output directory
	#	output_prefix: output prefix
	#	format: format of the lammps data file (default: molecular)
	set filename [file join ${output_dir} ${output_prefix}.lmpd]
	::TopoTools::topo writelammpsdata $filename $format -molid $molId -sel $sel_txt
	return
}