#====================================================
## ::cgfactory::tools::topology_builder_tools
# Goal: provide helper tools for building topology
# Author: Yuhang Wang
# Date: 11/18/2014

#====================================================

proc ::cgfactory::tools::topology::guess_bonds {molId {selection_str "all"} } {
	## Role: guess bonds
	#  Inputs:
	#	molId: molecule ID
	#	selection_str: atom selection string (default: "all")
	# note: VMD will add a bond for two atoms A and B
	#	if dAB < 0.6*(R_A + R_B)
	# 	where dAB is the distance between A and B
	#	R_A and R_B are radius of A and B, respectively.
	# see: https://sites.google.com/site/akohlmey/software/topotools/topotools-tutorial---part-1
	mol bondsrecalc $molId
	::TopoTools::topo retypebonds -molid $molId -sel $selection_str
	mol reanalyze $molId
	return
}

proc ::cgfactory::tools::topology::guess_angles {molId {selection_str "all"} } {
	## Role: guess angles
	::TopoTools::topo guessangles -molid $molId -sel $selection_str
	mol reanalyze $molId
	return
}

proc ::cgfactory::tools::topology::guess_dihedrals {molId {selection_str "all"}} {
	## Role: guess dihedral defintions for the selection atoms
	#  Note: this will totally reset the dihedral defintions for the selected atoms
	#	 but only if all the atoms in the new dihedral are contained in the 
	#	 current atom selection.
	#  Inputs:
	#	molId: VMD molecule ID
	#	selection_str: atom selecton string (default: "all")
	set sel [atomselect $molId $selection_str]
	set selected_atom_types [$sel get type]
	set selected_atom_indices [$sel list]

	set bonded_atom_indices_of_selected_atoms [$sel getbonds]
	set selected_bond_list [::cgfactory::tools::topology::get_bond_list $molId $selection_str]
	set new_dihedral_list {}
	
	## 1. Preserve any existing dihedrals that are related to the current 
	#  atom selection.
	
	# get all dihedrals for this molecule
	set all_dihedrals [::cgfactory::tools::topology::get_dihedral_list $molId "all"]
	
	foreach _dihedral $all_dihedrals {
		lassign $_dihedral type id1 id2 id3 id4
		# note: 
		#   -sorted flag tells lsearch to assume 
		#   the given list is already sorted in ascending order
		#   and to turn on an efficient searching algorithm
		#   -integer flag tells lsearch to use integer comparison
		
		# check whether any atom is outside the current atom selection
		# note: lsearch returns "-1" if item is not found in the list
		set criterion1 [expr [lsearch -sorted -integer $selected_atom_indices $id1] < 0 ]
		set criterion2 [expr [lsearch -sorted -integer $selected_atom_indices $id2] < 0 ]
		set criterion3 [expr [lsearch -sorted -integer $selected_atom_indices $id3] < 0 ]
		set criterion4 [expr [lsearch -sorted -integer $selected_atom_indices $id4] < 0 ]
		
		# Only if any atom is outside the current atom selection, add the involved dihedral into
		#  the list. In this way, there will be no duplication to the the newly created dihedral list
		if { $criterion1 || $criterion2 || $criterion3 || $criterion4 } {
			lappend new_dihedral_list $_dihedral
		}
	}
	
	## 2. for each bond, check whether there is any dihedral related to it
	# i.e. using it as the 2nd componet
	foreach _bond $selected_bond_list {
		lassign $_bond atomId1 atomId2
		
		# find out the order index of atom1/atom2 in the list
		set listIndex1 [lsearch -sorted -integer $selected_atom_indices $atomId1]
		set listIndex2 [lsearch -sorted -integer $selected_atom_indices $atomId2]
		
		set atomType1 [lindex $selected_atom_types $listIndex1]
		set atomType2 [lindex $selected_atom_types $listIndex2]
		set atomIdsBondedToAtom1 [lindex $bonded_atom_indices_of_selected_atoms $listIndex1]
		set atomIdsBondedToAtom2 [lindex $bonded_atom_indices_of_selected_atoms $listIndex2]
		
		# for every atom bonded to atom1
		foreach atomId3 $atomIdsBondedToAtom1 {
			# for every atom bonded to atom2
			foreach atomId4 $atomIdsBondedToAtom2 {
				# Make sure the bond atomId3--atomId1 and atomId4--atomId2 are
				# not the same as the bond atomId1--atomId2
				# Also make sure atomId3 != atomId4 (triangular shaped)
				set bad_case1 [expr ($atomId3 == $atomId1)]
				set bad_case2 [expr ($atomId3 == $atomId2)]
				set bad_case3 [expr ($atomId4 == $atomId1)]
				set bad_case4 [expr ($atomId4 == $atomId2)]
				set bad_case5 [expr ($atomId3 == $atomId4)] ;# bad triangular dihedral
				
				if {$bad_case1 || $bad_case2 || $bad_case3 || $bad_case4 || $bad_case5 } {
					continue
				}
				
				# find out the order index of atom1/atom2 in the list
				set listIndex3 [lsearch -sorted -integer $selected_atom_indices $atomId3]
				set listIndex4 [lsearch -sorted -integer $selected_atom_indices $atomId4]
				
				set atomType3 [lindex $selected_atom_types $listIndex3]
				set atomType4 [lindex $selected_atom_types $listIndex4]
				
				## Generate type name for the new dihedral
				#  and make sure it is alphabetically ascending
				set criterion1 [expr [string compare $atomType1 $atomType2] > 0]
				set sub_criterionA [expr [string compare $atomType1 $atomType2] == 0]
				set sub_criterionB [expr [string compare $atomType3 $atomType4] > 0]
				set criterion2 [expr $sub_criterionA && $sub_criterionB]
				
				set fourAtomTypes [list $atomType3 $atomType1 $atomType2 $atomType4]
				set fourAtomIds   [list $atomId3 $atomId1 $atomId2 $atomId4]
				
				# reverse the order if the original order is alphabetically desending
				if { $criterion1 || $criterion2 } {
					# i.e. "4-2-1-3"
					set fourAtomTypes [lreverse $fourAtomTypes]
					set fourAtomIds [lreverse $fourAtomIds]
				} 
				set new_dihedral_type_name [join $fourAtomTypes "-" ]
				lappend new_dihedral_list [linsert $fourAtomIds 0 $new_dihedral_type_name]
			}
		}
	}
	
	## update dihedral list
	::cgfactory::tools::topology::update_dihedral_list $molId $new_dihedral_list
	mol reanalyze $molId
	return
}

proc ::cgfactory::tools::topology::get_bond_list {molId {selection_str "all"} {option "none"} } {
	## Role: get a list of lists, each of which is
	#	a list of bonded-atom indices
	#  Inputs:
	#	molId: VMD molecular ID
	#	selection_str: atom selection string
	#	option: none | type | order | both (default: none)
	return [::TopoTools::topo getbondlist both -molid $molId -sel $selection_str]
}

proc ::cgfactory::tools::topology::get_angle_list {molId {selection_str "all"} } {
	## Role: return a list of angles
	#  Inputs:
	#  	molId: VMD mol ID
	#	selection_str: atom selection string (default: "all")
	return [::TopoTools::topo getanglelist -molid $molId -sel $selection_str]
}

proc ::cgfactory::tools::topology::get_dihedral_list {molId {selection_str "all"} } {
	## Role: return a list of dihedrals { {type id1 id2 id3 id4} ...}
	#  Inputs:
	#  	molId: VMD mol ID
	#	selection_str: atom selection string
	#
	return [::TopoTools::topo getdihedrallist -molid $molId -sel $selection_str]
}

proc ::cgfactory::tools::topology::update_bond_list {molId new_list {selection_str "all"} } {
	## Role: udpate the bond list
	#  Inputs:
	#	molId: molecule ID
	#	new_list: new bond list
	# 	selection_str: atom selection string (deafult: "all")
	::TopoTools::topo setbondlist both $new_list -molid $molId -sel $selection_str
	return
}

proc ::cgfactory::tools::topology::update_angle_list {molId new_list {selection_str "all"} } {
	## Role: udpate the angle list
	#  Inputs:
	#	molId: molecule ID
	#	new_list: new angle list
	# 	selection_str: atom selection string (deafult: "all")
	::TopoTools::topo setanglelist $new_list -molid $molId -sel $selection_str
	return
}

proc ::cgfactory::tools::topology::update_dihedral_list {molId new_list {selection_str "all"} } {
	## Role: udpate the dihedral list
	#  Inputs:
	#	molId: molecule ID
	#	new_list: new dihedral list
	# 	selection_str: atom selection string (deafult: "all")
	::TopoTools::topo setdihedrallist $new_list -molid $molId -sel $selection_str
	return
}

proc ::cgfactory::tools::topology::delete_angle {molId id1 id2 id3 {selection_str "all"} } {
	## Role: delete an angle
	#  Inputs:
	#  	molId: VMD mol ID
	#
	::TopoTools::topo delangle $id1 $id2 $id3 -molid $molId -sel $selection_str
	return
}

proc ::cgfactory::tools::topology::set_overlap {test_set target_set {SortedInt 0} } {
	## Role: check whether the test_set has any common items shared with target_set
	#  Inputs:
	#	test_set: a (usually small) set of items
	#	target_set: a (usally large) set of items
	#	SortedInt: a flag for specifying whether the target_set is a sorted integer list
	#		(in asending order).
	#		If so, then the lsearch -sorted -integer will be called to 
	#		optimize the performance.
	#	    (default: 0, i.e. not integer list)
	set HAS_OVERLAP 1
	set NO_OVERLAP  0
	foreach _item $test_set {
		if {$SortedInt == 0 } {
			set criterion [expr [lsearch $target_set $_item ] > 0]
		} else {
			set criterion [expr [lsearch -sorted -integer $target_set $_item] > 0]
		}
		
		if {$criterion} {
			return $HAS_OVERLAP
		}
	}
	return $NO_OVERLAP
}

proc ::cgfactory::tools::topology::retype_angles_by_degrees {molId {selection_str "all"} } {
	## Role: re-assign the angle types based on the angle degrees
	#  Inputs:
	#	molId: molId
	#	selection_str: atom selection string
	set all_angle_list [::cgfactory::tools::topology::get_angle_list $molId $selection_str]
	set new_angle_list {}
	
	# a dictionary with the degrees of the angle as key
	#  and the type of the angle as value
	set new_angle_type_dict {}
	
	foreach _item $all_angle_list {
		lassign $_item type id1 id2 id3

		set _degrees [measure angle [list $id1 $id2 $id3] molid $molId]
		
		## in order to faciliate searching, conver _degrees to int
		set _degrees [expr round(${_degrees}) ]
		set keys_angle_dict [dict keys $new_angle_type_dict]
		
		## when this _degrees is not found in the old dictionary
		# add it to the dictionary
		if {[lsearch $keys_angle_dict $_degrees] == -1 } {
			set n_keys [llength $keys_angle_dict]
			incr n_keys
			set _new_angle_type "${type}-${_degrees}"
			dict append new_angle_type_dict $_degrees $_new_angle_type
		} else {
			set _new_angle_type [dict get $new_angle_type_dict $_degrees]
		}
		set _new_item [list $_new_angle_type $id1 $id2 $id3]
		lappend new_angle_list $_new_item
	}
	
	# update angle list
	::cgfactory::tools::topology::update_angle_list $molId $new_angle_list $selection_str
}

proc ::cgfactory::tools::topology::retype_dihedrals_by_degrees {molId {selection_str "all"} } {
	## Role: re-assign the dihedral types based on the dihedral angle degrees
	#	If selection_str is specified, then only dihedrals involving the selected atoms
	#	are modified
	#  Inputs:
	#	molId: molId
	#	selection_str: atom selection string
	set all_dihedral_list [::cgfactory::tools::topology::get_dihedral_list $molId $selection_str]
	set new_dihedral_list {}
	
	# a dictionary with the degrees of the dihedral as key
	#  and the type of the dihedral as value
	set new_dihedral_type_dict {}
	
	foreach _item $all_dihedral_list {
		lassign $_item type id1 id2 id3 id4
		
		set _degrees [measure dihed [list $id1 $id2 $id3 $id4] molid $molId]
		## in order to faciliate searching, conver _dihedral to int
		# note: for dihedrals, we only need positive angles
		#  0 and 180 are the same dihedrals
		set _degrees [expr abs(round(${_degrees})) % 180 ]
		set keys_dihedral_dict [dict keys $new_dihedral_type_dict]
		
		## when this _dihedral is not found in the old dictionary
		# add it to the dictionary
		if {[lsearch $keys_dihedral_dict $_degrees] == -1 } {
			set n_keys [llength $keys_dihedral_dict]
			incr n_keys
			set _new_dihedral_type "${type}-${_degrees}"
			dict append new_dihedral_type_dict $_degrees $_new_dihedral_type
		} else {
			set _new_dihedral_type [dict get $new_dihedral_type_dict $_degrees]
		}
		set _new_item [list $_new_dihedral_type $id1 $id2 $id3 $id4]
		lappend new_dihedral_list $_new_item
	}
	
	# update dihedral list
	::cgfactory::tools::topology::update_dihedral_list $molId $new_dihedral_list $selection_str
}

proc ::cgfactory::tools::topology::delete_dihedral {molId id1 id2 id3 id4} {
	## Role: delete a dihedral angle
	#  Inputs:
	#  	molId: VMD mol ID
	#
	::TopoTools::topo deldihedral $id1 $id2 $id3 $id4 -molid $molId
	return
}

proc ::cgfactory::tools::topology::delete_angle_types {molId bad_angle_types {selection_str "all"} } {
	## Role: delete angles using fast batch mode
	# Inputs:
	#	molId: VMD mol ID
	#	bad_angle_types: list angle types to be deleted
	#	selection_str: atom selection string (default: "all")
	puts "== delete bad angle types"
	set selected_angles [::cgfactory::tools::topology::get_angle_list $molId $selection_str]
	set new_angle_list {}
	puts "bad angle types: $bad_angle_types"
	
	foreach _angle $selected_angles {
		lassign $_angle angle_type id1 id2 id3
		set angle_type_forward $angle_type
		set criterion1 [lsearch $bad_angle_types $angle_type_forward]
		if { $criterion1 == -1 } {
			lappend new_angle_list $_angle
		} else {
			puts "=== delete bad angle type: $angle_type"
		}
	}
	
	## update angle list
	::cgfactory::tools::topology::update_angle_list $molId $new_angle_list $selection_str
	return
}

proc ::cgfactory::tools::topology::batch_add_new_bonds {molId bond_list} {
	## Add a list of new angles to the existing bond list
	#  INPUTS:
	#	molId: VMD molecule ID
	#	new_anlge_list: a list of new bonds, e.g. {{1 2 A-A 1.0} {2 3 A-B 1.0}}
	#
	set old_bond_list [::cgfactory::tools::topology::get_bond_list $molId]
	set updated_bond_list [concat $old_bond_list $bond_list]
	# update
	puts "=== $updated_bond_list"
	::cgfactory::tools::topology::update_bond_list $molId $updated_bond_list
	puts [::cgfactory::tools::topology::get_bond_list $molId]
	return
}

proc ::cgfactory::tools::topology::batch_add_new_angles {molId new_angle_list} {
	## Add a list of new angles to the existing angle list
	#  INPUTS:
	#	molId: VMD molecule ID
	#	new_anlge_list: a list of new angles, e.g. {{type 1 2 3} {type 2 3 4}}
	#
	set old_angle_list [::cgfactory::tools::topology::get_angle_list $molId]
	set updated_angle_list [concat $old_angle_list $new_angle_list]
	# update
	::cgfactory::tools::topology::update_angle_list $molId $updated_angle_list
	return
}

proc ::cgfactory::tools::topology::delete_dihedral_types {molId bad_dihedral_types {selection_str "all"} } {
	## Role: delete dihedrals using fast batch mode
	# Inputs:
	#	molId: VMD mol ID
	#	bad_dihedral_types: list dihedral types to be deleted
	#	selection_str: atom selection string (default: "all")
	puts "== delete bad dihedral types"
	set selected_dihedrals [::cgfactory::tools::topology::get_dihedral_list $molId $selection_str]
	set new_dihedral_list {}
	
	foreach _dihedral $selected_dihedrals {
		lassign $_dihedral dihedral_type id1 id2 id3 id4
		set dihedral_type_forward $dihedral_type
		set dihedral_type_reverse [join [lreverse [split $dihedral_type "-"]] "-"]
		set criterion1 [lsearch $bad_dihedral_types $dihedral_type_forward]
		set criterion2 [lsearch $bad_dihedral_types $dihedral_type_reverse]
		if { $criterion1 == -1 && $criterion2 == -1 } {
			lappend new_dihedral_list $_dihedral
		} else {
			puts "=== delete bad dihedral type: $dihedral_type"
		}
	}
	
	## update dihedral list
	::cgfactory::tools::topology::update_dihedral_list $molId $new_dihedral_list $selection_str
	return
}

proc ::cgfactory::tools::topology::rm_dihedral_duplicates {molId {selection_str "all"} } {
	## Role: remove duplicated dihedrals 
	#  Inputs:
	#	molId: VMD molecule ID
	#	selection_str: atom selection string (default: "all")
	set selected_dihedral_items [::cgfactory::tools::topology::get_dihedral_list $molId $selection_str]
	set new_dihedral_items {}
	
	# build a list of dihedrals identified by their atom members
	set dihedral_atom_groups {}
	
	foreach _item $selected_dihedral_items {
		lassign $_item type id1 id2 id3 id4
		set id_list [list $id1 $id2 $id3 $id4]
		set id_list_reverse [lreverse $id_list]
		set groupId1 [join $id_list "-"]
		set groupId2 [join $id_list_reverse "-"]
		set criterion1 [lsearch $dihedral_atom_groups $groupId1]
		set criterion2 [lsearch $dihedral_atom_groups $groupId2]
		
		# only add this item to the new dihedral list when
		# neither the forward nor the reserve atom list is found
		if {$criterion1 == -1 && $criterion2 == -1 } {
			lappend new_dihedral_items $_item
		} else {
			puts "found dihedral duplicates: $groupId1 "
		}
	}
	
	# update dihedral list
	::cgfactory::tools::topology::update_dihedral_list $molId $new_dihedral_items $selection_str
}

proc ::cgfactory::tools::topology::add_bond {molId atomid1 atomid2 bondtype {bondorder 1} } {
	## Role: add bond between two atoms
	#  Inputs:
	#	molId: VMD mol ID
	#	atomid1: index for the 1st atom (0-based)
	#	atomid2: index for the 2nd atom (0-based)
	#	bondtype: specify bond type
	#	bondorder: bond order (default: 1)
	#
	::TopoTools::topo addbond $atomid1 $atomid2 -bondtype $bondtype -bondorder $bondorder -molid $molId
	puts "== add new bond: ${atomid1}--${atomid2}"
	return
}

proc ::cgfactory::tools::topology::get_bonded_partner_indices {molId atomid} {
	## Role: return a list of atom indices for the bonded partners of atom with index $atomid
	#  Inputs:
	#	molId: VMD mol ID
	#	atomid: atom index
	#
	set _sel [atomselect $molId "index $atomid"]
	set raw_list_of_lists [$_sel getbonds]
	# since only one atom is selected, we only need the first entry from raw_list_of_lists
	set output [lindex $raw_list_of_lists 0]
	$_sel delete
	return $output
}


proc ::cgfactory::tools::topology::get_bonded_neighbor_indices {molId selection_str} {
	## Role: get the atom indices the the bonded neighbors of selected atoms
	#  Inputs:
	#	molId: VMD molecule ID
	#	selection_str: atom selection string
	set sel [atomselect $molId $selection_str]
	set neighbor_index_list [$sel getbonds]
	$sel delete
	return $neighbor_index_list
}

proc ::cgfactory::tools::topology::find_long_bond {molId atomId_target atomIds_neighbor max_bond_length} {
	## Role: find the bond whose length is greater than max_bond_length
	#  Inputs:
	#	molId: VMD mol ID
	#	atomId_target: atom index of the target atom
	#	atomIds_neighbor: atom indices of the neighbors of the target
	#	max_bond_length: maximum bond length
	#
	set long_bonds {}
	set updated_bonded_indices {} 
	set target [atomselect $molId "index $atomId_target "]
	set coord_target [ lindex [$target get {x y z}] 0 ] 
	foreach _atomid_neighbor $atomIds_neighbor { 
		set _neighbor [ atomselect $molId "index $_atomid_neighbor" ] 
		set _coord_neighbor [ lindex [$_neighbor get {x y z}] 0 ] 
		set _bond_length [ vecdist $coord_target $_coord_neighbor ]
		if {  $_bond_length < $max_bond_length } { 
			lappend updated_bonded_indices $_atomid_neighbor
		} else {
			lappend long_bonds [list $atomId_target $_atomid_neighbor]
		}
		$_neighbor delete
	     } 
	$target setbonds [ list $updated_bonded_indices ] 
	return $long_bonds
}



proc ::cgfactory::tools::topology::add_graphene_dihedrals {molId N_per_residue N_residues dihedral_type_name} {
	## Role: add dihedrals to a graphene sheet
	#  Inputs:
	#	molId: VMD molecule ID
	#	N_per_residue: number of beads per graphene residue (i.e. one row of beads)
	#	N_residues: number of graphene residues
	#	dihedral_type_name: name of the dihedral type to be created
	set new_dihedral_list {}
	
	# define starting/ending number(exclusive) for atom ID list1
	for {set i 0} {$i < [expr $N_residues - 1] } {incr i} {
		set list1_start [expr $i*$N_per_residue]
		set list1_end [expr $list1_start + $N_per_residue]
		set list2_start [expr $list1_end ]
		set list2_end [expr $list2_start + $N_per_residue]
		
		set list1 [::cgfactory::tools::list::range $list1_start $list1_end]
		set list2 [::cgfactory::tools::list::range $list2_start $list2_end]
		
		if {[expr ${i}%2] == 0 } {
			# dihedral between odd graphene residue followed by even residue
			set _dihedrals [::cgfactory::tools::topology::create_dihedrals $dihedral_type_name $list1 $list2]
		} else {
			# dihedral between even graphene residue followed by odd residue
			set _dihedrals [::cgfactory::tools::topology::create_dihedrals $dihedral_type_name $list2 $list1]
		}
		
		foreach _dihe $_dihedrals {
			lappend new_dihedral_list $_dihe
		}
		
		## add additional dihedrals between neighboring diamond shapes
		if {$N_per_residue >= 3 } {
			if {[expr $i%2] == 0} {
        			set additional_list1 [lrange $list2 0 [expr $N_per_residue - 2] ]
        			set additional_list2 [lrange $list1 1 [expr $N_per_residue - 1] ]
			} else {
				set additional_list1 [lrange $list1 0 [expr $N_per_residue - 2] ]
				set additional_list2 [lrange $list2 1 [expr $N_per_residue - 1] ]
			}
			
			set additional_dihedrals [::cgfactory::tools::topology::create_dihedrals $dihedral_type_name \
			 	$additional_list1 $additional_list2]
			
			foreach _dihe $additional_dihedrals {
				puts "==== additional dihedral: $_dihe"
				lappend new_dihedral_list $_dihe
			}
		}
		
		
		## add hinge dihdrals if there are more than 3 graphene residues
		if {$N_residues >= 3 && $i < [expr $N_residues - 2] } {
			set list3_start [expr $list2_end]
			set list3_end [expr $list3_start + $N_per_residue]
			set list3 [::cgfactory::tools::list::range $list3_start $list3_end]
			set hinge_dihedrals [::cgfactory::tools::topology::create_graphene_hinge_dihedrals $dihedral_type_name \
				 $list1 $list2 $list3]
			foreach _dihe $hinge_dihedrals {
				puts "new hinge dihedral: $_dihe "
				lappend new_dihedral_list $_dihe
			}
		}
		
	}
	::cgfactory::tools::topology::update_dihedral_list $molId $new_dihedral_list
	return
}


proc ::cgfactory::tools::topology::create_dihedrals {dihedral_type_name id_list1 id_list2} {
	## Role: make a  list of new dihedrals
	#  Inputs:
	#	dihedral_type_name: name of the dihedral
	#	id_list1: a list of atom IDs
	#	id_list2: a list of atom IDs
	set N [llength $id_list1]
	set dihedral_list {}
	for {set i 0} {$i < [expr $N - 1]} {incr i} {
		set id1 [lindex $id_list1 $i]
		set id2 [lindex $id_list1 [expr $i + 1]]
		set id3 [lindex $id_list2 $i]
		set id4 [lindex $id_list2 [expr $i + 1]]
		set new_dihedral [list $id1 $id2 $id3 $id4]
		if {$id2 > $id3} {
			# make sure the new dihedral is in asending order 
			# in terms of atom ids
			set new_dihedral [lreverse $new_dihedral]
		}
		set new_dihedral [linsert $new_dihedral 0 $dihedral_type_name]
		lappend dihedral_list $new_dihedral
	}
	return $dihedral_list
}

proc ::cgfactory::tools::topology::create_graphene_hinge_dihedrals {dihedral_type_name id_list1 id_list2 id_list3} {
	## Role: add hing like dihedrals for 3 rows of graphene beads
	#  Inputs:
	#	dihedral_type_name: name of the graphene dihedral type
	#	id_list1: list of atom indices for the 1st row of graphene beads
	#	id_list2: list of atom indices for the 2nd row of graphene beads
	#	id_list3: list of atom indices for the 3rd row of graphene beads
	# return: a dihedral list
	set length1 [llength $id_list1]
	set length2 [llength $id_list2]
	set length3 [llength $id_list3]
	
	set dihedral_list {}
	
	if { ! $length1 >= 2 } {
		puts "ERROR HINT: input list must have length >=2 (from create_graphene_hinge_dihedrals)"
	}
	
	if {$length1 != $length2} {
		puts "ERROR HINT: input list1 must be of the same length as list2 (from create_graphene_hinge_dihedrals)"
	}
	
	if {$length1 != $length3} {
		puts "ERROR HINT: input list1 must be of the same length as list3 (from create_graphene_hinge_dihedrals)"
	}
	
	if {$length2 != $length3} {
		puts "ERROR HINT: input list2 must be of the same length as list3 (from create_graphene_hinge_dihedrals)"
	}
	
	
	for {set i 0} {$i < [expr $length1 - 1]} {incr i} {
		set id1 [lindex $id_list1 $i]
		set id2 [lindex $id_list2 $i]
		set id3 [lindex $id_list2 [expr $i+1]]
		set id4 [lindex $id_list3 $i]
		set new_dihedral [list $id1 $id2 $id3 $id4]
		if {$id2 > $id3} {
			# make sure the new dihedral is in asending order 
			# in terms of atom ids
			set new_dihedral [lreverse $new_dihedral]
		}
		set new_dihedral [linsert $new_dihedral 0 $dihedral_type_name]
		lappend dihedral_list $new_dihedral
	}
	
	return $dihedral_list
}


proc ::cgfactory::tools::topology::add_graphene_oxide_dihedrals {molId selection_str new_dihedral_type} {
	## Role: add dihedrals to the -OH bead
	#  Inputs:
	#	molId: VMD molecule ID
	#	selection_str: atom selection string for the -OH bead
	#	new_dihedral_type: name of the new dihedral type 
	
	# note: OHG represent the -OH bead
	set sel [atomselect $molId $selection_str]
	set OHG_atomIds [$sel get index]
	$sel delete
	
	## preserve old dihedrals
	set new_dihedral_list [::cgfactory::tools::topology::get_dihedral_list $molId]
	
	foreach id1 $OHG_atomIds  {
		set new_dihedral {}
		lappend new_dihedral $id1
		
		set sel_str "index $id1"
		# find the atom index for OHG's immediate neighbor
		set id1_neighbor_indices [lindex [::cgfactory::tools::topology::get_bonded_neighbor_indices $molId $sel_str] 0]
		set forbidden_indices [concat $OHG_atomIds $new_dihedral]
		set id2 [::cgfactory::tools::topology::get_item $id1_neighbor_indices $forbidden_indices 1]
		lappend new_dihedral $id2
		
		# now find two neighbors of id2 which is not id1
		# Also, angle id3-id2-id4 should be 60 degrees 
		# (120 or 180 degress may lead to unstable dihedrals)
		set sel_str "index $id2"
		set id2_neighbor_indices [lindex [::cgfactory::tools::topology::get_bonded_neighbor_indices $molId $sel_str] 0]
		# Developer's note: the forbidden_indices is used to make sure that the new tmp_id 
		# is not one of the OHG beads and not the same as any bead already
		# added to $new_dihedral
		set forbidden_indices [concat $OHG_atomIds $new_dihedral]
		lassign [::cgfactory::tools::topology::get_item $id2_neighbor_indices $forbidden_indices 1] id3
		lappend new_dihedral $id3
		set forbidden_indices [concat $OHG_atomIds $new_dihedral] ;# update to include id3
		
		## find id4 such that angle id3-id2-id4 == 60 degrees
		set ccc 0 ;# counter
		set max_iter [llength $id2_neighbor_indices]
		while {1} {
			lassign [::cgfactory::tools::topology::get_item $id2_neighbor_indices $forbidden_indices 1] id4
			set _angle [measure angle [list $id3 $id2 $id4] molid $molId]
			set _angle [expr round($_angle)]
			if {$_angle  == 60 } {
				lappend new_dihedral $id4
				break
			} else {
				lappend forbidden_indices $id4
			}
			if {$ccc > $max_iter} {break} ;# prevent accidental infinit loop
			incr ccc
		}
		
		# raise an error if id4 couldn't be found
		if {[llength $new_dihedral] != 4} {
			set msg "ERROR HINT: could not create dihedral for OHG bead of index $id1 "
			error $msg
		}
		
		
		set new_dihedral [linsert $new_dihedral 0 $new_dihedral_type]
		
		lappend new_dihedral_list $new_dihedral
	}
	
	::cgfactory::tools::topology::update_dihedral_list $molId $new_dihedral_list
	return
}

proc ::cgfactory::tools::topology::get_item {sample_set forbidden_set {number_of_outputs 1} } {
	## Role: select the 1st item from sample_set that is not in forbidden_set
	#  Inputs:
	#	sample_set: set of items to be tested
	#	forbidden_set: set of items that are not allowed to be returned
	#	number_of_outputs: number of indices to output (default: 1)
	#  Return: first item in sample_set that is not in forbidden_set
	#	or return string "NONE" if none is found
	set output {}
	
	foreach _item $sample_set {
		if {[lsearch $forbidden_set $_item ] == -1 } {
			lappend output $_item
		}
		
		if { [llength $output] == $number_of_outputs} {
			break
		}
	}
	return $output
}

