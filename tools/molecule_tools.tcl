#====================================================
## ::cgfactory::tools::merge_mol_tools
# Role: mege multiple molecules into one
# Author: Yuhang Wang
# Date: 11/19/2014

#====================================================

proc ::cgfactory::tools::molecule::mergemols {molIdList} {
	## Role: merge multiple molecules into one
	set newMolId [::TopoTools::mergemols $molIdList]
	if {$newMolId == -1 } {
		puts "ERROR HINT: could not merge the molecules in the list: $molIdList"
	}
	return $newMolId
}

proc ::cgfactory::tools::molecule::delete_mols {molIdList} {
	## Role: garbage collect molecules that are no longer needed
	foreach molid $molIdList {
		mol delete $molid
	}
}

proc ::cgfactory::tools::molecule::replicate {molId Nx Ny Nz {resid_start 1} } {
	## Role: replicate the building block along X, Y, and Z
	#  Inputs:
	#	molId: VMD mol ID
	#	Nx: number of replicates along X
	#	Ny: cf. Nx
	#	Nz: cf. Nz
	#	resid_start: starting residue ID (default: 1)
	#
	set newMolId [::TopoTools::replicatemol $molId $Nx $Ny $Nz]
	
	## renumber resId's
	::cgfactory::tools::molecule::renumber_resid $newMolId $resid_start
	
	return $newMolId
}

proc ::cgfactory::tools::molecule::renumber_resid {molId {resid_start 1} } {
	## Role: renumber the molecule's residue IDs, assuming only
	#	one type of residue exist in the system
	#  Inputs:
	#	molId: VMD mol ID
	#	resid_start: starting residue ID (default: 1)
	set atom0 [atomselect $molId "index 0"]
	
	set first_resnum [lindex [$atom0 get residue] 0]
	set n_residues [::cgfactory::tools::molecule::get_num_residues $molId]
	for {set i 0} {$i < $n_residues} {incr i} {
		set _resnum [expr $first_resnum + $i]
		set _new_resid [expr $resid_start + $i]
		set _sel [atomselect $molId "residue $_resnum "]
		$_sel set resid $_new_resid
		$_sel delete
	}
	$atom0 delete
	return 
}

proc ::cgfactory::tools::molecule::get_num_residues {molId } {
	## Role: get number of residues, assuming only one
	#	type of residue exist.
	#  Inputs:
	#	molId: VMD mol ID
	#
	set residue0 [atomselect $molId "residue 0"]
	set n_per_residue [$residue0 num]
	set all [atomselect $molId "all"]
	set n_all [$all num]
	set n_residues [expr $n_all/$n_per_residue]
	## garbage collection
	$residue0 delete
	$all delete
	return $n_residues
}
