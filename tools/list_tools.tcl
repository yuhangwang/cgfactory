## ::cgfactory::tools::list
# Goal: tools for manipulating Tcl lists
# Author: Yuhang Wang
# Date: 11/23/2014

#====================================================

proc ::cgfactory::tools::list::rm_2d_duplicates_unordered {in_list} {
	## Role: remove duplicated entries in a Nx2 list of lists
	#	 in an unordered fashion, i.e. {a b} and {b a} 
	#	are considered the same.
	#  Inputs:
	#	in_list: input 2D list of lists; each element has 2 items
	#
	set new_list {}
	foreach _list1 $in_list {
		set _list2 [lreverse $_list1]
		## if neither forward or reverse list exists in new_list, add it
		set criterion1 [expr [lsearch $new_list  $_list1] == -1]
		set criterion2 [expr [lsearch $new_list  $_list2] == -1]
		if { $criterion1 && $criterion2 } {
			lappend new_list $_list1
		}
	}
	return $new_list
}

proc ::cgfactory::tools::list::range {n_one n_end} {
	## Role: make a list between n_one and n_end(exclusive)
	#  Inputs:
	# 	n_one: starting number
	#	n_end: last number + 1
	# note: same as range() in python
	set List {}
	puts "$n_one $n_end "
	for {set i $n_one} {$i < $n_end } {incr i} {
		lappend List $i
	}
	return $List
}

proc ::cgfactory::tools::list::wrap_list {in_list n} {
	## wrap a list by moving the last $n items to the beginning of the list
	#  INPUTS:
	#	in_list: input list
	#	n: number of items to be wrapped
	#  RETURN: a new list
	#  EXAMPLE: wrap_list {0 1 2 3} 1 yields {3 0 1 2}
	#
	set len [llength $in_list]
	set part1 [lrange $in_list [expr $len-$n] [expr $len - 1] ]
	set part2 [lrange $in_list 0 [expr $len-$n-1]]
	set output [concat $part1 $part2]
	return $output
}