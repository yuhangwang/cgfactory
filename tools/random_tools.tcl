#====================================================
## ::cgfactory::tools::rand_tools
# Goal: a suite of random number generator tool set
# Author: Yuhang Wang
# Date: 11/20/2014

#====================================================

proc ::cgfactory::tools::random::get_a_random_int {lower upper} {
	## Role: generate one random integer
	#	between lower and upper
	#  Inputs:
	#	lower: lower bound
	#	upper: upper bound
	#	n_total: total number of random integers
	#
	set L [expr $upper - $lower + 1]
	set tmp [::tcl::mathfunc::int [::tcl::mathfunc::floor [expr rand()*$L]]] ;# a temporary integer between 0 and (L-1)
	return [expr $lower + $tmp]
}

proc ::cgfactory::tools::random::pick_items {in_list N} {
	## Role: randomly pick n items from the in_list
	#  Inputs:
	# 	in_list: input list
	#	N: total number of items to be picked
	#
	set LOOPMAX 10000000000 ;# maximum looping count (prevent inf loop)
	set output {}
	set lenList [llength $in_list]
	if {$N>$lenList} {
		puts "ERROR HINT: you are trying to pick $N items from a list of size $lenList"
		return -1
	}
	# upper/lower bound for the indices
	set i_lower 0
	set i_upper [expr $lenList - 1]
	set rand_seq {}
	set ccc 0 ;# counter
	while {$ccc < $N} {
		set _rand [::cgfactory::tools::random::get_a_random_int $i_lower $i_upper]
		# only keep the new random int if it is a brand new integer
		if {[lsearch $rand_seq $_rand] == -1 } {
			lappend rand_seq $_rand
			incr ccc
		}
	}
	
	## pick the elements
	foreach _i $rand_seq {
		lappend output [lindex $in_list $_i]	
	}
	
	return $output
}