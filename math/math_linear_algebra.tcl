## Basic linear algebra operations
# Yuhang Wang
# 04-05-2015


proc ::cgfactory::math::vector_dot {v1 v2} {
	## Compute the dot product of two vectors
	# Inputs:
	# v1: vector 1
	# v2: vector 2
	set N [llength $v1]
	set N2 [llength $v2]
	if {$N != $N2} {
		set msg "ERROR FROM ::cgfactory::math::vector_dot\n"
		append msg "ERROR: input vectors v1 and v2 must have the same length"
		puts $msg
	}
	
	set sum [expr 0.0]
	for {set i 0} {$i < $N} {incr i} {
		set n1 [lindex $v1 $i]
		set n2 [lindex $v2 $i]
		set sum [expr $sum + $n1*$n2 ]
	}
	return $sum
}

proc ::cgfactory::math::matrix_multiply {M v} {
	## Multiply a matrix M and a vector v
	# Inputs:
	# M: matrix (2D list of lists)
	# v: vector
	set N [llength $M]
	set N2 [llength $v]
	if {$N != $N2} {
		set msg "ERROR FROM ::cgfactory::math::matrix_multiply\n"
		append msg "ERROR: input matrix M and vector v must have the same length"
		puts $msg
	}
	
	set output_vector {}
	
	for {set i 0 } {$i < $N} {incr i} {
		set v1 [lindex $M $i]
		lappend output_vector [::cgfactory::math::vector_dot $v1 $v]
	}
	
	return $output_vector
}
