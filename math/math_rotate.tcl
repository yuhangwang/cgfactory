## Rotate a vector by some angle
# Yuhang Wang
# 04/05/2015



proc ::cgfactory::math::rotate2D {in_vector angle_degrees} {
	## Rotate an input vector by some degrees
	# Inputs:
	# in_vector: input 3-vector, but only the first 2 element will be modifed
	# angle_degrees: angle in degrees
	
	lassign $in_vector x y z
	set vector2D [list $x $y]
	set tail [lrange $in_vector 2 end]
	
	# Define the rotation matrix
	set angle_radian [::cgfactory::math::unit::degree2radian $angle_degrees]
	set M11 [expr cos($angle_radian)]
	set M12 [expr -sin($angle_radian)]
	set M21 [expr sin($angle_radian)]
	set M22 [expr cos($angle_radian)]
	set M_rotate [list [list $M11 $M12] [list $M21 $M22] ]
	
	set new_vector2D [::cgfactory::math::matrix_multiply $M_rotate $vector2D]
	
	set output_vector [concat $new_vector2D $tail]
	
	return $output_vector
}