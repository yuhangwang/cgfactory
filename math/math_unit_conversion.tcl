## functions for unit conversion

proc ::cgfactory::math::unit::radian2degree {rad} {
	## Convert angle in radian to degrees
	return [expr $rad*180./$::cgfactory::math::constants::Pi]
}

proc ::cgfactory::math::unit::degree2radian {deg} {
	## Convert angle in radian to degrees
	return [expr $deg*$::cgfactory::math::constants::Pi/180.]
}