## Mathematical constants

namespace eval ::cgfactory::math::constants {
	variable Pi [expr 3.14159265358979]
}
