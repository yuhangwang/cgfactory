## ::cgfactory::math::basics
# Goal: provide extension to the built-in math library
# Author: Yuhang Wang
# Date: 11/23/2014

#====================================================


proc ::cgfactory::math::basics::sign x {expr {($x>0) - ($x<0)}}

proc ::cgfactory::math::basics::isEven x {expr $x%2 == 0}

proc ::cgfactory::math::basics::isOdd x {expr $x%2 == 1}