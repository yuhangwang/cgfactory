# create the namespace
namespace eval ::helloTcl {
	# export commands
	variable version 0.0.0
	variable HELLODIR "/Users/stevenw/workspace/eclipse_Luna_Java/VMDTclProject/cgraphene"
	namespace export   hello
}

package provide helloTcl $::helloTcl::version

proc ::helloTcl::hello {name} {
	puts "hello $name"
}

