# Package: cgfactory
# Goal: provide a set of tools for building coarse-grained model
# Author: Yuhang Wang
# Date: 11/09/14
#====================================================
namespace eval ::cgfactory {
	variable version 0.0.1
	variable CGF_home $env(CGFactoryDir)
	variable CGF_test_dir  [file join $::cgfactory::CGF_home "tests"]
	variable CGF_data_dir  [file join $::cgfactory::CGF_home "data"]
	variable CGF_tools_dir [file join $::cgfactory::CGF_home "tools"]
	variable CGF_constants_dir [file join $::cgfactory::CGF_home "constants"]
	variable CGF_graphene_dir [file join $CGF_home "graphene"]
	variable CGF_lipid_dir [file join $CGF_home "lipid"]
	variable CGF_protein_dir [file join $CGF_home "protein"]
	variable CGF_oxidize_dir [file join $CGF_home "oxidize"]
	variable CGF_visual_dir [file join $CGF_home "visual"]
	variable CGF_math_dir [file join $CGF_home "math"]
	variable CGF_unitconvert_dir [file join $CGF_home "unitconvert"]
	
	namespace export hello
	namespace export get_src_dir get_test_dir
	#===========================================================
	# define constants
	namespace eval constants {
		namespace export *
		namespace ensemble create
	}
	#-------------------------
	namespace export constants
	#===========================================================
	# plugin: tools
	namespace eval tools {
		namespace eval pbc {
			namespace export *
			namespace ensemble create
		}
		namespace eval system {
			namespace export *
			namespace ensemble create
		}
		namespace eval select {
			namespace export *
			namespace ensemble create
		}
		namespace eval param {
			namespace export *
			namespace ensemble create
		}
		namespace eval coordinate {
			namespace export *
			namespace ensemble create
		}
		namespace eval io {
			namespace export *
			namespace ensemble create
		}
		namespace eval make {
			namespace export *
			namespace ensemble create
		}
		namespace eval molecule {
			namespace export *
			namespace ensemble create
		}
		namespace eval pbbc {
			namespace export *
			namespace ensemble create
		}
		namespace eval random {
			namespace export *
			namespace ensemble create
		}
		namespace eval topology {
			namespace export *
			namespace ensemble create
		}
		namespace eval distance {
			namespace export *
			namespace ensemble create
		}
		namespace eval print {
			namespace export *
			namespace ensemble create
		}
		namespace eval list {
			namespace export *
			namespace ensemble create
		}
		namespace eval measure {
			namespace export *
			namespace ensemble create
		}
		
		namespace export *
		namespace ensemble create
	}
	#-------------------------
	namespace export tools
	#===========================================================
	# plugin: graphene
	namespace eval graphene {
		namespace export *
		namespace ensemble create
	}
	#-------------------------
	namespace export graphene
	#===========================================================
	# plugin: lipid
	namespace eval lipid {
		namespace export *
		namespace ensemble create
	}
	#-------------------------
	namespace export lipid
	#===========================================================
	# plugin: lipid
	namespace eval protein {
		namespace export *
		namespace ensemble create
	}
	#-------------------------
	namespace export protein
	#===========================================================
	# plugin: oxidize
	namespace eval oxidize {
		namespace export *
		namespace ensemble create
	}
	
	#-------------------------
	namespace export oxidize
	#===========================================================
	# plugin: visual
	namespace eval visual {
		namespace export *
		namespace ensemble create
	}
	#-------------------------
	namespace export visual
	#===========================================================
	# plugin: math
	namespace eval math {
		namespace eval basics {
			namespace export *
			namespace ensemble create
		}
		namespace eval constants {
			namespace export *
			namespace ensemble create
		}
		namespace eval unit {
			namespace export *
			namespace ensemble create
		}
		namespace export *
		namespace ensemble create
	}
	#-------------------------
	namespace export math
	#===========================================================
	# plugin: unitconvert
	namespace eval unitconvert {
		namespace eval dpd {
			namespace export *
			namespace ensemble create
		}
		namespace export *
		namespace ensemble create
	}
	#-------------------------
	namespace export unitconvert
}
package provide cgfactory $::cgfactory::version

#====================================================
# External Package Dependencies
#====================================================
package require topotools
package require pbctools

#====================================================


#=================================================================
## CGFactory getters
#=================================================================
proc ::cgfactory::hello {name} {
	puts "hello $name"
	puts "CGF_home:   	$::cgfactory::CGF_home"
	puts "CGF_graphene_dir:	$::cgfactory::CGF_graphene_dir"
	puts [glob -directory	$::cgfactory::CGF_graphene_dir *]
}

proc ::cgfactory::get_src_dir {} {
	## Role: return the CGFactory package installation directory
	return $::cgfactory::CGF_home
}

proc ::cgfactory::get_test_dir {} {
	## Role: return the test suite directory for CGFactory
	return $::cgfactory::CGF_test_dir
}

proc ::cgfactory::get_data_dir {} {
	## Role: return the data directory inside this package
	return $::cgfactory::CGF_data_dir
}

proc ::cgfactory::get_one_atom_file {} {
	## Role: return the file name of 
	return [file join $::cgfactory::CGF_data_dir "one_atom.xyz"]
}

proc ::cgfactory::get_constants_dir {} {
	## Role:  return the directory for cgfactory::constants
	return $::cgfactory::CGF_constants_dir
}

proc ::cgfactory::get_graphene_dir {} {
	## Role: return the directory for the subpackage "graphene"
	return $::cgfactory::CGF_graphene_dir
}

proc ::cgfactory::get_lipid_dir {} {
	## Role: return the directory for the subpackage "lipid"
	return $::cgfactory::CGF_lipid_dir
}

proc ::cgfactory::get_protein_dir {} {
	## Role: return the directory for the subpackage "lipid"
	return $::cgfactory::CGF_protein_dir
}

proc ::cgfactory::get_oxidize_dir {} {
	## Role: return the directory for the subpackage "oxidize"
	return $::cgfactory::CGF_oxidize_dir
}

proc ::cgfactory::get_visual_dir {} {
	## Role: return the directory for the subpackage "visual"
	return $::cgfactory::CGF_visual_dir
}

proc ::cgfactory::get_math_dir {} {
	## Role: return the directory for the subpackage "math"
	return $::cgfactory::CGF_math_dir
}

proc ::cgfactory::get_unitconvert_dir {} {
	## Role: return the directory for the subpackage "unitconvert"
	return $::cgfactory::CGF_unitconvert_dir
}

#=================================================================
#  Define constants
#=================================================================
source [file join $::cgfactory::CGF_constants_dir "cgfactory_constants.tcl"]

#=================================================================
#  Install common utility  tools
#=================================================================
source [file join $::cgfactory::CGF_tools_dir "coordinate_tools.tcl"]
source [file join $::cgfactory::CGF_tools_dir "param_tools.tcl"]
source [file join $::cgfactory::CGF_tools_dir "topology_tools.tcl"]
source [file join $::cgfactory::CGF_tools_dir "system_tools.tcl"]
source [file join $::cgfactory::CGF_tools_dir "pbc_tools.tcl"]
source [file join $::cgfactory::CGF_tools_dir "io_tools.tcl"]
source [file join $::cgfactory::CGF_tools_dir "make_tools.tcl"]
source [file join $::cgfactory::CGF_tools_dir "molecule_tools.tcl"]
source [file join $::cgfactory::CGF_tools_dir "random_tools.tcl"]
source [file join $::cgfactory::CGF_tools_dir "pbbc_tools.tcl"]
source [file join $::cgfactory::CGF_tools_dir "select_tools.tcl"]
source [file join $::cgfactory::CGF_tools_dir "distance_tools.tcl"]
source [file join $::cgfactory::CGF_tools_dir "print_tools.tcl"]
source [file join $::cgfactory::CGF_tools_dir "list_tools.tcl"]
source [file join $::cgfactory::CGF_tools_dir "measure_tools.tcl"]


#=================================================================
#  Install oxidize module
#=================================================================
# Build Oxidized Coarse-Grained Graphene
#-------------------------
source [file join $::cgfactory::CGF_oxidize_dir "oxidize_namespace.tcl"]
source [file join $::cgfactory::CGF_oxidize_dir "oxidize.tcl"]
source [file join $::cgfactory::CGF_oxidize_dir "oxidize_by_mutation.tcl"]

#=================================================================
#  Install graphene builder module
#=================================================================
# Build Coarse-Grained Graphene
#-------------------------
source [file join $::cgfactory::CGF_graphene_dir "graphene_namespace.tcl"]
source [file join $::cgfactory::CGF_graphene_dir "build_graphene.tcl"]
source [file join $::cgfactory::CGF_graphene_dir "build_graphene_oxide.tcl"]
source [file join $::cgfactory::CGF_graphene_dir "build_graphene_with_hydrophilic_mutations.tcl"]


#=================================================================
#  Install lipid builder module
#=================================================================
# Build lipid
#-------------------------
source [file join $::cgfactory::CGF_lipid_dir "lipid_namespace.tcl"]
source [file join $::cgfactory::CGF_lipid_dir "build_dppc.tcl"]


#=================================================================
#  Install protein module
#=================================================================
# Build protein
#-------------------------
source [file join $::cgfactory::CGF_protein_dir "protein_namespace.tcl"]
source [file join $::cgfactory::CGF_protein_dir "protein_make_one_bead.tcl"]
source [file join $::cgfactory::CGF_protein_dir "protein_make_one_chain.tcl"]
source [file join $::cgfactory::CGF_protein_dir "build_membrane_protein.tcl"]
source [file join $::cgfactory::CGF_protein_dir "build_channel_protein.tcl"]


#=================================================================
#  Install visual module
#=================================================================
# visualization tools
#-------------------------
source [file join $::cgfactory::CGF_visual_dir "visual_pbbc.tcl"]



#=================================================================
#  Install math module
#=================================================================
# math tools
#-------------------------
source [file join $::cgfactory::CGF_math_dir "math_basics.tcl"]
source [file join $::cgfactory::CGF_math_dir "math_constants.tcl"]
source [file join $::cgfactory::CGF_math_dir "math_unit_conversion.tcl"]
source [file join $::cgfactory::CGF_math_dir "math_linear_algebra.tcl"]
source [file join $::cgfactory::CGF_math_dir "math_rotate.tcl"]

#=================================================================
#  Install unitconvert module
#=================================================================
#  simulation unit conversions
#-------------------------
source [file join $::cgfactory::CGF_unitconvert_dir "unitconvert_dpd.tcl"]


