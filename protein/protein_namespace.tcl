# Role: provide namespaces specific to ::cgfactory::protein
# Author: Yuhang Wang
# Date: 11/17/2014


namespace eval ::cgfactory::protein {
	variable ResName "MP"
	variable AvgAminoAcidMass 110
	variable NumResiduesPerBead 3.6
	variable BondNameList {"PRO-PRO"}
	variable AngleNameList {"PRO-PRO-PRO"}
	variable PROTEIN_BOND_LENGTH     5.4 ;# rise per turn in alpha-helix
	set scaleFactor 0.8
	variable InterLayerDistance  [expr $scaleFactor*$PROTEIN_BOND_LENGTH]
}

namespace eval ::cgfactory::protein::TYPE {
	# PA: hydrophilic protein bead
	# PB: hydrophobic protein bead
	variable PA "AP"
	variable PB "BP"
}

namespace eval ::cgfactory::protein::NAME {
	# PA: hydrophilic protein bead
	# PB: hydrophobic protein bead
	variable PA "AP"
	variable PB "BP"
}

namespace eval ::cgfactory::protein::MASS {
	# Role: conventions for atom masses
	set MassProteinBead [expr $::cgfactory::protein::NumResiduesPerBead*$::cgfactory::protein::AvgAminoAcidMass]
	variable PA $MassProteinBead
	variable PB $MassProteinBead
}

namespace eval ::cgfactory::protein::CHARGE {
	# Role: conventions for atom masses
	variable PA 0.0
	variable PB 0.0
}

proc ::cgfactory::protein::get_unit_box_size {} {
	## Role: return the size of the unit box
	#	for the protein bead
	#	and also the length along X per carbon
	set Lx $::cgfactory::protein::PROTEIN_BOND_LENGTH
	set Ly $::cgfactory::protein::PROTEIN_BOND_LENGTH
	set Lz $::cgfactory::protein::PROTEIN_BOND_LENGTH
	return [list $Lx $Ly $Lz]
}