## Build a 3-layer membrane protein
# Yuhang Wang
# 04-04-15

source [file join [::cgfactory::get_protein_dir] "protein_make_one_circular_layer.tcl"]

proc ::cgfactory::protein::build_membrane_protein {n_beads_per_chain {inner_hydrophilic_per_end 3} {outer_hydrophilic_per_end 3} {n_layers 3} } {
	## Build a 3-layer cylindrical membrane channel protein
	# Inputs:
	# n_beads_per_chain: number of beads per protein chain
	# inner_hydrophilic_per_end: number of hydrophilic beads per end for the inner most layer (default: 3)
	# outer_hydrophilic_per_end: number of hydrophilic beads per end for the outer most layer (default: 3)
	# n_layers: number of protein layers (default: 3)
	
	set list_molIds {}
	
	puts "flag 1"
	set list_nDivisions {}
	for {set i 0} {$i < $n_layers} {incr i} {
		lappend list_nDivisions [expr 6*(2**$i)]
	}
	
	puts "flag 2"
	set list_nHydrophilic [list $inner_hydrophilic_per_end]
	
	for {set i 1} {$i < $n_layers} {incr i} {
		lappend list_nHydrophilic $outer_hydrophilic_per_end
	}
	
	puts "nDivision list: $list_nDivisions"
	puts "nHydrophilic list: $list_nHydrophilic"

	set ccc 0

	foreach n_divisions $list_nDivisions {
        	set segn_prefix "PRO${ccc}"
		set n_hydrophilic_beads_per_end [lindex $list_nHydrophilic $ccc]
		set radius [expr $::cgfactory::protein::InterLayerDistance*($ccc+1)]
		set layer_id $ccc
        	lassign [::cgfactory::protein::make_one_circular_layer $n_beads_per_chain $radius $n_divisions $segn_prefix $n_hydrophilic_beads_per_end $layer_id] molId
		lappend list_molIds $molId
		incr ccc
	}
	
	## merge all residue into one molecule
	set newMolId [::cgfactory::tools::molecule::mergemols $list_molIds]
	
	set X_extra_margin [expr 0]
	set Y_extra_margin [expr 0]
	set Z_extra_margin [expr 0]
	::cgfactory::graphene::set_pbc_box_size $newMolId $X_extra_margin $Y_extra_margin $Z_extra_margin
	
	## garbage collect un-needed intermediate molecules
	::cgfactory::tools::molecule::delete_mols $list_molIds
	
	
	return $newMolId
}
