## Make one circular layer of protein beads
# Yuhang Wang
# 04-04-2015

source [file join [::cgfactory::get_protein_dir] "protein_make_one_bead.tcl"]

proc ::cgfactory::protein::make_one_circular_layer {n_beads_per_chain radius n_divisions segname_prefix n_hydrophilic_beads_per_end layer_id} {
	## Make a circular layer of protein beads
	# Inputs:
	# n_beads_per_chain: number of beads per protein chain
	# radius: radius of the circle
	# n_divisions: number of divisions of the circle 
	# segname_prefix: prefix for the segname of each new chain
	# n_hydrophilic_beads_per_end: number of hydrophilic beads per end
	# layer_id: an ID for this protein layer
	
	set bead_type_suffix $layer_id
	set list_molIds {}
	set list_segnames {}
	set delta_angle [expr 360/$n_divisions]
	set tol [expr 1E-5]
	for {set i 0 } {$i < $n_divisions} {incr i} {
		set new_segname ${segname_prefix}$i
		lappend list_segnames $new_segname
		lassign [::cgfactory::protein::build_one_chain $n_beads_per_chain $new_segname $n_hydrophilic_beads_per_end $bead_type_suffix] molId
		lappend list_molIds $molId
		
		# if there is only chain to build, then no need to rotate
		if {[expr ($n_divisions - 1) < $tol]} {
			puts "n_divisions = $n_divisions"
			return $molId 
		}
		
		set angle_degrees [expr $i*$delta_angle]
		set v0 [list $radius 0 0] ;# starting translation vector
		set translation_vector [::cgfactory::math::rotate2D $v0 $angle_degrees]
		lassign $translation_vector dx dy dz
		
		# move the protein chain to a new location
		::cgfactory::tools::coordinate::move $molId $dx $dy $dz
	}
	
	## merge all residue into one molecule
	set newMolId [::cgfactory::tools::molecule::mergemols $list_molIds]
	
	## add bonds between segments
	set new_bond_name "${segname_prefix}-${segname_prefix}"
	::cgfactory::protein::add_bonds_between_segments $newMolId $list_segnames $new_bond_name
	
	set X_extra_margin [expr 0]
	set Y_extra_margin [expr 0]
	set Z_extra_margin [expr 0]
	::cgfactory::graphene::set_pbc_box_size $newMolId $X_extra_margin $Y_extra_margin $Z_extra_margin
	
	## garbage collect un-needed intermediate molecules
	::cgfactory::tools::molecule::delete_mols $list_molIds
	
	return $newMolId
}

proc ::cgfactory::protein::add_bonds_between_segments {molId list_segnames new_bond_name {new_bond_order 1.0} } {
	## add bonds in a protein chain
	# Inputs:
	# molId: VMD molecular ID
	# new_bond_name: name of the new bond
	# new_bond_order: 1.0 of single bond; 2.0 for double bond
	
	set N [llength $list_segnames]
	set list_new_bonds {}
	for {set i 0} {$i < [expr $N]} {incr i} {
		
		if {$i == [expr $N-1]} {
			# add bonds between the last and the 1st segments
			# in order to complete the circular arrangement
			set j 0
		} else {
			set j [expr $i+1]
		}
		
		set segn1 [lindex $list_segnames $i]
		set segn2 [lindex $list_segnames $j]
		
		set sel1 [atomselect $molId "segname $segn1"]
		set sel2 [atomselect $molId "segname $segn2"]
		set list_index1 [$sel1 get index]
		set list_index2 [$sel2 get index]
		$sel1 delete
		$sel2 delete
		
		set N1 [llength $list_index1]
		set N2 [llength $list_index2]
		
		if {$N1 != $N2} {
			set msg "ERROR: from ::cgfactory::protein::add_bonds_between_segments \n"
			append msg "ERROR: 2 protein segments must have the same number of atoms"
			puts $msg
		}
		
		## build a new bond list
		for {set k 0 } {$k <  $N1} {incr k} {
			set id1 [lindex $list_index1 $k]
			set id2 [lindex $list_index2 $k]
			set new_bond [list $id1 $id2 $new_bond_name $new_bond_order]
			lappend list_new_bonds $new_bond
		}
	}
	::cgfactory::tools::topology::batch_add_new_bonds $molId $list_new_bonds
	return
}
