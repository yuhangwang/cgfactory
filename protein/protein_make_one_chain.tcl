## Build one protein chain (ref.: Venturoli et. al. Biop. J. 88:1778, 2005)
# Yuhang Wang
# 04/04/2015

source [file join [::cgfactory::get_protein_dir] "protein_make_one_bead.tcl"]

proc ::cgfactory::protein::build_one_chain {n_beads new_segname n_hydrophilic_beads_per_end bead_type_suffix } {
	## Build a single protein chain
	# Inputs:
	# n_beads: number of beads in a protein chain
	# new_segname: the segment name for a protein chain
	# bead_type_suffix: suffix for new protein beads
	
	lassign [::cgfactory::protein::make_one_bead $new_segname $bead_type_suffix] molId oneAtom
	lassign [::cgfactory::protein::get_unit_box_size] Lx Ly Lz
	::cgfactory::tools::pbc::set_box_size $molId $Lx $Ly $Lz
	
	set Nx [expr 1]
	set Ny [expr 1]
	set Nz $n_beads
	set newMolId [::cgfactory::tools::molecule::replicate $molId $Nx $Ny $Nz]
	
	## update PBC box sizes
	set Lx [expr $Nx*$::cgfactory::protein::PROTEIN_BOND_LENGTH]
	set Ly $::cgfactory::protein::PROTEIN_BOND_LENGTH
	set Lz $::cgfactory::protein::PROTEIN_BOND_LENGTH
	::cgfactory::tools::pbc::set_box_size $newMolId $Lx $Ly $Lz
	
	## recenter the molecule to be around the origin
	::cgfactory::tools::coordinate::recenter $newMolId
	
	## add bonds
	set new_bond_name [lindex $::cgfactory::protein::BondNameList 0]
	::cgfactory::protein::add_bonds_in_a_chain $newMolId $new_bond_name
	
	## add angles
	set new_angle_name [lindex $::cgfactory::protein::AngleNameList 0]
	::cgfactory::protein::add_angles_in_a_chain $newMolId $new_angle_name
	
	## Mutate first/last n beads into hydrophilic beads
	::cgfactory::protein::mutate_ends_to_hydrophic $newMolId $n_hydrophilic_beads_per_end $bead_type_suffix
	
	return [list $newMolId]
}


proc ::cgfactory::protein::mutate_ends_to_hydrophic {molId n_hydrophilic_beads_per_end bead_type_suffix} {
	## Mutate the first and last n beads into hydrophilic beads
	# Inputs:
	# molId: VMD molecular ID
	# n_hydrophilic_beads_per_end: number of hydrophilic beads per ends
	# bead_type_suffix: suffix the bead types
	set all [atomselect $molId all]
	set list_index [$all get index]
	$all delete
	
	set N [llength $list_index]
	
	set beg 0
	set end [expr $n_hydrophilic_beads_per_end - 1]
	set group1_indices [lrange $list_index $beg $end ]
	
	set beg [expr $N - $n_hydrophilic_beads_per_end]
	set end [expr $N - 1]
	set group2_indices [lrange $list_index $beg $end ]
	
	set list_ids [concat $group1_indices $group2_indices]
	foreach id $list_ids {
		set oneAtom [atomselect $molId "index $id"]
		$oneAtom set type   "${::cgfactory::protein::TYPE::PA}${bead_type_suffix}"
		$oneAtom set name   $::cgfactory::protein::NAME::PA
		$oneAtom set mass   $::cgfactory::protein::MASS::PA
		$oneAtom set charge $::cgfactory::protein::CHARGE::PA
		$oneAtom delete
	}
	return 
}

proc ::cgfactory::protein::add_bonds_in_a_chain {molId new_bond_name {new_bond_order 1.0} } {
	## add bonds in a protein chain
	# Inputs:
	# molId: VMD molecular ID
	# new_bond_name: name of the new bond
	# new_bond_order: 1.0 of single bond; 2.0 for double bond
	
	set list_new_bonds {}
	set all [atomselect $molId all]
	set list_index [$all get index]
	set N [llength $list_index]
	for {set i 0} {$i < [expr $N - 1 ]} {incr i} {
		set j [expr $i+1]
		set id1 [lindex $list_index $i]
		set id2 [lindex $list_index $j]
		set new_bond [list $id1 $id2 $new_bond_name $new_bond_order]
		lappend list_new_bonds $new_bond
	}
	::cgfactory::tools::topology::batch_add_new_bonds $molId $list_new_bonds
	return
}

proc ::cgfactory::protein::add_angles_in_a_chain {molId new_angle_name } {
	## add bonds in a protein chain
	# Inputs:
	# molId: VMD molecular ID
	# new_angle_name: name of the new angle
	
	set list_new_angles {}
	set all [atomselect $molId all]
	set list_index [$all get index]
	$all delete
	set N [llength $list_index]
	for {set i 0} {$i < [expr $N - 2 ]} {incr i} {
		set j [expr $i+1]
		set k [expr $i+2]
		set id1 [lindex $list_index $i]
		set id2 [lindex $list_index $j]
		set id3 [lindex $list_index $k]
		set new_angle [list $new_angle_name $id1 $id2 $id3]
		lappend list_new_angles $new_angle
	}
	::cgfactory::tools::topology::batch_add_new_angles $molId $list_new_angles
	return
}