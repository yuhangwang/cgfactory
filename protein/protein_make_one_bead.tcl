#====================================================
## ::cgfactory::tools::make_one_protein_bead
# Goal: make one CG protein bead
# Author: Yuhang Wang
# Date: 04/04/2014

#====================================================

proc ::cgfactory::protein::make_one_bead { {segname "P"} {bead_type_suffix ""} } {
	## Role: build a single CG protein carbon atom
	# Inputs:
	# segname: segment name
	set molId [::cgfactory::tools::make::make_one_bead]
	set oneAtom [atomselect $molId all]
	$oneAtom set type   "${::cgfactory::protein::TYPE::PB}${bead_type_suffix}"
	$oneAtom set name   $::cgfactory::protein::NAME::PB
	$oneAtom set mass   $::cgfactory::protein::MASS::PB
	$oneAtom set charge $::cgfactory::protein::CHARGE::PB
	$oneAtom set radius $::cgfactory::protein::PROTEIN_BOND_LENGTH
	$oneAtom set segname $segname
	$oneAtom set resname $::cgfactory::protein::ResName
	return [list $molId $oneAtom]
}