## Goal: convert to DPD unit system
#  Author: Yuhang Wang
#  Date: 11/26/2014
#


proc ::cgfactory::unitconvert::dpd::akma2dpd { molId {length_dpd2akma 8.0} {mass_dpd2akma 54.0} } {
	## Role: convert from AKMA to DPD unit system
	#  Inputs
	#  molId: VMD mol ID
	#  length_dpd2akma: 1 DPD length unit = ? Angstroms (default: 8.0)
	#  mass_dpd2akma: 1 DPD mass unit = ? atomic mass units (default: 54.0)
	#
	set all [atomselect $molId all]
	
	set allAtomIds [$all get index]
	set length_akma2dpd [expr 1.0/$length_dpd2akma]
	set mass_akma2dpd [expr 1.0/$mass_dpd2akma]
	## For each atom, scale its coordinates
	foreach _atomid $allAtomIds {
		set _sel [atomselect $molId "index $_atomid"]
		
		## convert mass
		set _mass [$_sel get mass]
		set _new_mass [expr $mass_akma2dpd*$_mass]
		$_sel set mass $_new_mass
		
		## convert xyz
		set _old_xyz [lindex [$_sel get {x y z}] 0]
		lassign [vecscale $length_akma2dpd $_old_xyz] x y z
		$_sel set x $x
		$_sel set y $y
		$_sel set z $z
		
		## garbage collection
		$_sel delete
	}
	
	## scale PBC box size
	lassign [molinfo $molId get {a b c}] Lx Ly Lz
	molinfo $molId set a [expr $length_akma2dpd*$Lx]
	molinfo $molId set b [expr $length_akma2dpd*$Ly]
	molinfo $molId set c [expr $length_akma2dpd*$Lz]
	
	## garbage collection
	$all delete
	return
}
