# Goal: test basic import for package "cgfactory"
# Author: Yuhang Wang
# Date: 11/17/2014

set ext_dir "/Users/stevenw/GitCode"
lappend auto_path $ext_dir
package require cgfactory
namespace import ::cgfactory::*

set output [hello "steven"]

puts "dir: [get_src_dir]"
puts "test: [get_test_dir]"

exit;