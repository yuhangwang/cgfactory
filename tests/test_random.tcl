# Goal: test rand_tools.tcl
# Author: Yuhang Wang
# Date: 11/20/2014

set ext_dir "/Users/stevenw/GitCode"
lappend auto_path $ext_dir

package require cgfactory
namespace import ::cgfactory::tools

set inList {0 1 2 3 4 5 6}
set N 3
puts "rand seq 1: [::cgfactory::tools::random::pick_items $inList $N]"
puts "rand seq 2: [::cgfactory::tools::random::pick_items $inList $N]"
puts "rand seq 3: [::cgfactory::tools::random::pick_items $inList $N]"

exit