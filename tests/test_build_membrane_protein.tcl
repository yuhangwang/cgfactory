# Goal: test build_3layer_membrane_protein
# Author: Yuhang Wang
# Date: 11/17/2014

set ext_dir "/Users/stevenw/GitCode"
lappend auto_path $ext_dir

package require cgfactory
namespace import ::cgfactory::protein

set output_dir "/Users/stevenw/Downloads"
set output_prefix "pro"
set resid_start 1
set segname_prefix "PRO"
set chainId "A"
set Lx 16
set Ly 15
set Lz 0.0
set n_beads_per_chain 11
set molId [protein build_membrane_protein $n_beads_per_chain]

## Save pdb/psf
::cgfactory::tools::io::save_pdb $output_dir $output_prefix $molId
::cgfactory::tools::io::save_psf $output_dir $output_prefix $molId
::cgfactory::tools::io::save_lmpd $output_dir $output_prefix $molId

exit


