#====================================================
## test: build a CG POPE molecule
# Author: Yuhang Wang
# Date: 11/19/2014

#====================================================
set ext_dir "/Users/stevenw/GitCode"
lappend auto_path $ext_dir

package require cgfactory
namespace import ::cgfactory::lipid

set output_dir "/Users/stevenw/Downloads"
set output_prefix "dppc"

## Run and build
set molId [lipid build_dppc ]

set Nx 1
set Ny 1
set Nz 1
set molId [::cgfactory::tools::molecule::replicate $molId $Nx $Ny $Nz]

## Save pdb/psf
::cgfactory::tools::io::save_pdb $output_dir $output_prefix $molId
::cgfactory::tools::io::save_psf $output_dir $output_prefix $molId
::cgfactory::tools::io::save_xml $output_dir $output_prefix $molId
::cgfactory::tools::io::save_lmpd $output_dir $output_prefix $molId

exit

