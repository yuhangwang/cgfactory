#====================================================
## test: ::cgfactory::tools::distance module
# Author: Yuhang Wang
# Date: 11/23/2014

#====================================================
set ext_dir "/Users/stevenw/GitCode"
lappend auto_path $ext_dir

package require cgfactory
namespace import ::cgfactory::tools::distance

set v1 {0 0 1}
set v2 {1 0 0}
set v3 {2 1 0}

set v_list1 [list $v1 $v2 $v3]
set dist [distance measure_distance $v1 $v2]
array set pairdist [distance pairwise_distances $v_list1 $v_list1]

puts "distance: $dist\n"

for {set i 0} {$i < [llength $v_list1]} {incr i} {
	for {set j 0} {$j < [llength $v_list1]} {incr j} {
		puts -nonewline "$pairdist($i,$j)\t"
	}
	puts ""
}
exit