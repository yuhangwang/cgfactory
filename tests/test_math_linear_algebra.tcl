## Test 
# ::cgfactory::math::vector_dot
# ::cgfactory::math::matrix_multiply
# Yuhang Wang
# 04-05-2014

set ext_dir "/Users/stevenw/GitCode"
lappend auto_path $ext_dir

package require cgfactory
namespace import ::cgfactory::math

set v1 {1 1 1}
set v2 {0 1 0}
set out [::cgfactory::math::vector_dot $v1 $v2]
puts "dot product of v1: {$v1} and v2: {$v2} = $out"


set M {{1 1} {0 1}}
set v {1 2}
set out_v [::cgfactory::math::matrix_multiply $M $v]
puts "M = $M"
puts "v = $v"
puts "M*v = $out_v"
exit

