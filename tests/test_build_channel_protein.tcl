# Goal: test build_3layer_membrane_protein
# Author: Yuhang Wang
# Date: 11/17/2014

set ext_dir "/Users/stevenw/GitCode"
lappend auto_path $ext_dir

package require cgfactory
namespace import ::cgfactory::protein

set output_dir "/Users/stevenw/Downloads"
set output_prefix "pro"
set resid_start 1
set segname_prefix "PRO"
set n_beads_per_chain 11
set inner_pore_radius 10
set n_hydrophilic_per_end 5
set molId [protein build_channel_protein $n_beads_per_chain $inner_pore_radius $n_hydrophilic_per_end]

## Save pdb/psf
::cgfactory::tools::io::save_pdb $output_dir $output_prefix $molId
::cgfactory::tools::io::save_psf $output_dir $output_prefix $molId
::cgfactory::tools::io::save_lmpd $output_dir $output_prefix $molId

exit


