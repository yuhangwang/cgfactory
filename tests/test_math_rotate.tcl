## Test 
# ::cgfactory::math::rotate
# Yuhang Wang
# 04-05-2014

set ext_dir "/Users/stevenw/GitCode"
lappend auto_path $ext_dir

package require cgfactory
namespace import ::cgfactory::math

set v {1 0 1}
set angle 360
set out [::cgfactory::math::rotate $v $angle]
puts "v = $v"
puts "After rotation of $angle degrees, v = $out"

exit

