#====================================================
## test: ::cgfactory::tools::coordinate module
# Author: Yuhang Wang
# Date: 11/23/2014

#====================================================
set ext_dir "/Users/stevenw/GitCode"
lappend auto_path $ext_dir

package require cgfactory
namespace import ::cgfactory::tools::coordinate
namespace import ::cgfactory::tools::make

set molId [make make_one_bead]
set axis "x"
set atomIds [list 0]
set coords [coordinate get_coords1D $molId $atomIds $axis]
puts "output coordiantes: $coords"
exit




