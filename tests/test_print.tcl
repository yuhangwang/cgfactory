#====================================================
## test: ::cgfactory::tools::print module
# Author: Yuhang Wang
# Date: 11/23/2014

#====================================================
set ext_dir "/Users/stevenw/GitCode"
lappend auto_path $ext_dir

package require cgfactory

array set array2D {0,0 1 0,1 2
		   1,0 3 1,1 4
}

set n_rows 2
set n_cols 2

::cgfactory::tools::print::array2D "array2D" $n_rows $n_cols

exit
