## Test the create graphene hinge dihedrals


set ext_dir "/Users/stevenw/GitCode"
lappend auto_path $ext_dir
puts $auto_path

package require cgfactory

puts "test range"
set length 3
set n_one 0
set n_end $length
set list1 [::cgfactory::tools::list::range $n_one $n_end]

set n_one $n_end
set n_end [expr $n_one + $length]
set list2 [::cgfactory::tools::list::range $n_one $n_end]

set n_one $n_end
set n_end [expr $n_one + $length]
set list3 [::cgfactory::tools::list::range $n_one $n_end]

set dihedral_name "C-C-C-C"
set dihedrals [::cgfactory::tools::topology::create_graphene_hinge_dihedrals $dihedral_name $list1 $list2 $list3]

puts "new dihedrals"
foreach _dihedral $dihedrals {
	puts $_dihedral
}


exit


