# Goal: test build_cg_graphene
# Author: Yuhang Wang
# Date: 11/17/2014

set ext_dir "/Users/stevenw/GitCode"
lappend auto_path $ext_dir

package require cgfactory
namespace import ::cgfactory::graphene

set output_dir "/Users/stevenw/Downloads"
set output_prefix "gra"
set resid_start 2
set segname_prefix "GC"
set chainId "C"
set Lx 16
set Ly 15
set Lz 0.0
set molId [graphene build_graphene $Lx $Ly $Lz $resid_start $segname_prefix $chainId]

## Save pdb/psf
::cgfactory::tools::io::save_pdb $output_dir $output_prefix $molId
::cgfactory::tools::io::save_psf $output_dir $output_prefix $molId
::cgfactory::tools::io::save_lmpd $output_dir $output_prefix $molId

exit


