#====================================================
## test: ::cgfactory::tools::pbc module
# Author: Yuhang Wang
# Date: 11/23/2014

#====================================================
set ext_dir "/Users/stevenw/GitCode"
lappend auto_path $ext_dir

package require cgfactory
namespace import ::cgfactory::tools::coordinate


set dist [expr 5.0]
set boxL [expr 6.0]
set wrapped [::cgfactory::tools::pbc::min_image_distance $dist $boxL]


puts "wrapped distance: $wrapped from original distance $dist"

exit
