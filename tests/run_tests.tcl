# Goal: run TDD test cases
# author: Yuhang Wang
# date: 11/09/14

set runCaseId 22
set ext_dir "/Applications/VMD1.9.2.app/Contents/vmd/plugins/noarch/tcl"
lappend auto_path $ext_dir

package require cgfactory

set test_dir [::cgfactory::get_test_dir]
set log_dir "~/Downloads"
set tmplog [file join $log_dir "tmp.log"]

proc show {filename} {
	set f_in [open $filename r];
	set content [read $f_in];
	puts $content;
	close $f_in;
}

proc vmde {script output_filename} {
	set vmd_dir "/Applications/VMD1.9.2.app/Contents/vmd"
	set vmd [file join $vmd_dir "vmd"]
	exec $vmd -dispdev text -nt -e $script > $output_filename
#	exec $vmd -nt -e $script 
}

switch -- $runCaseId {
	1  {puts "case 1: hello"; set testfilename "test_hello.tcl"}
	2  {puts "case 2: build_graphene"; set testfilename "test_build_graphene.tcl";}
	3  {puts "case 3: build DPPC"; set testfilename "test_build_dppc.tcl"}
	4  {puts "case 4: build graphene oxide"; set testfilename "test_build_graphene_oxide.tcl"}
	5  {puts "case 5: test  rand"; set testfilename "test_random.tcl"}
	6  {puts "case 6: test pbbc"; set testfilename "test_pbbc.tcl"}
	7  {puts "case 7: test distance"; set testfilename "test_distance.tcl"}
	8  {puts "case 8: test coordinate"; set testfilename "test_coordinate.tcl"}
	9  {puts "case 9: test pbc"; set testfilename "test_pbc.tcl"}
	10 {puts "case 10: test print"; set testfilename "test_print.tcl"}
	11 {puts "case 11: test visual::show_pbbc"; set testfilename "test_visual_pbbc.tcl"}
	12 {puts "case 12: test pbbc with graphene oxide"; set testfilename "test_pbbc_go.tcl"}
	13 {puts "case 13: test unitconvet dpd"; set testfilename "test_unitconvert_dpd.tcl"}
	14 {puts "case 14: test range"; set testfilename "test_range.tcl"}
	15 {puts "case 15: test create dihedrals"; set testfilename "test_create_dihedrals.tcl"}
	16 {puts "case 16: test create graphene hinge dihedrals"; set testfilename "test_create_graphene_hinge_dihedrals.tcl"}
	17 {puts "case 17: build graphene with hydrophilic beads"; set testfilename "test_build_graphene_with_hydrophilic_beads.tcl"}
	18 {puts "case 18: build graphene with  PBBC"; set testfilename "test_build_graphene_with_pbbc.tcl"}
	19 {puts "case 19: build membrane protein"; set testfilename "test_build_membrane_protein.tcl"}
	20 {puts "case 20: math linear algebra"; set testfilename "test_math_linear_algebra.tcl"}
	21 {puts "case 22: math rotate"; set testfilename "test_math_rotate.tcl"}
	22 {puts "case 22: build channel protein"; set testfilename "test_build_channel_protein.tcl"}
}

set script [file join $test_dir $testfilename]
vmde $script $tmplog;
show $tmplog;



