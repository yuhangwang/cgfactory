# Goal: test build_cg_graphene by mutations
# Author: Yuhang Wang
# Date: 11/17/2014

set ext_dir "/Users/stevenw/GitCode"
lappend auto_path $ext_dir

package require cgfactory
namespace import ::cgfactory::graphene

set output_dir "/Users/stevenw/Downloads"
set output_prefix "gog"
set resid_start 2
set segname_prefix "GC"
set chainId "C"
set topBottom 1
set Lx 50
set Ly 50
set Lz 0
set oxyLevel 0.5 ;# oxidation level
set molId [graphene build_graphene_with_hydrophilic_mutations $Lx $Ly $Lz $topBottom $oxyLevel $resid_start $segname_prefix $chainId]

## Save pdb/psf
puts "save pdf/psf"
::cgfactory::tools::io::save_pdb $output_dir $output_prefix $molId
::cgfactory::tools::io::save_psf $output_dir $output_prefix $molId
::cgfactory::tools::io::save_lmpd $output_dir $output_prefix $molId

exit


