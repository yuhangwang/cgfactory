## Test the range function


set ext_dir "/Users/stevenw/GitCode"
lappend auto_path $ext_dir
puts $auto_path

package require cgfactory

puts "test range"
set n_one 0
set n_end 2
set list1 [::cgfactory::tools::list::range $n_one $n_end]

set n_one 2
set n_end 4
set list2 [::cgfactory::tools::list::range $n_one $n_end]

set dihedral_name "C-C-C-C"
set dihedrals [::cgfactory::tools::topology::create_dihedrals $dihedral_name $list1 $list2]

puts "new dihedrals"
puts $dihedrals
foreach _dihedral $dihedrals {
	puts $_dihedral
}


exit


