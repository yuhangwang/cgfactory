#====================================================
## Test: testing visual::show_pbbc
# Author: Yuhang Wang
# Date: 11/23/2014

#====================================================
set ext_dir "/Users/stevenw/GitCode"
lappend auto_path $ext_dir

package require cgfactory
namespace import ::cgfactory::tools
namespace import ::cgfactory::graphene


set output_dir "/Users/stevenw/Downloads"
set output_prefix "pbbc"
set resid_start 2
set segname_prefix "GC"
set chainId "C"
set Lx 20
set Ly 20
set Lz 4.0
set tol 0.2
set molId [graphene build_graphene $Lx $Ly $Lz $resid_start $segname_prefix $chainId]

set bond_length_cutoff $::cgfactory::graphene::CC_BOND_LENGTH
set selection_str "segname GC1 and name CGC"
set bond_type "CGC-CGC"

## get PBC box info
lassign [::cgfactory::tools::pbc::get_box_size $molId] boxLx boxLy boxLz
puts "boxLx: $boxLx; boxLy: $boxLy; boxLz: $boxLz"
set boxL [list $boxLx $boxLy $boxLz]
## get system min max
lassign [::cgfactory::tools::system::measure_minmax $molId $selection_str] xmin ymin zmin xmax ymax zmax

set pbc_axis "x"
set pbc_length $boxLx
set target_selection_str [::cgfactory::tools::select::band_str $pbc_axis $xmin $tol $selection_str]
set target [atomselect $molId $target_selection_str]
puts "==== target num [$target num]"
set atomIds_target [$target get index]

set neighbor_selection_str [::cgfactory::tools::select::band_str $pbc_axis $xmax $tol $selection_str]
set neighbor [atomselect $molId $neighbor_selection_str]
puts "==== neighbor num [$neighbor num]"
set atomIds_neighbor [$neighbor get index]

puts "=== CALL pbbc::setup_pbbc ==="
::cgfactory::tools::pbbc::setup_pbbc $molId $atomIds_target $atomIds_neighbor $boxL  $bond_length_cutoff $bond_type $tol



## use visual::show_pbbc
set selection_str "all"
set cutoff 5.0 
::cgfactory::visual::show_pbbc $molId $selection_str 5.0 

exit