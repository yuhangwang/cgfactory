#====================================================
## Test: testing PBBC
# Author: Yuhang Wang
# Date: 11/21/2014

#====================================================
set ext_dir "/Users/stevenw/GitCode"
lappend auto_path $ext_dir

package require cgfactory
namespace import ::cgfactory::tools
namespace import ::cgfactory::graphene

set output_dir "/Users/stevenw/Downloads"
set output_prefix "pbbc"
set resid_start 2
set segname_prefix "GC"
set chainId "C"
set Lx 20
set Ly 20
set Lz 4.0
set tol 0.2
set oxyLevel 0.5
set topBottom 1
set molId [graphene build_graphene_oxide $Lx $Ly $Lz $topBottom $oxyLevel $resid_start $segname_prefix $chainId]


set bond_length_cutoff $::cgfactory::graphene::CC_BOND_LENGTH
set selection_str "segname GC1 and name CGC"
set bond_type "CGC-CGC"

## get PBC box info
lassign [::cgfactory::tools::pbc::get_box_size $molId] boxLx boxLy boxLz
puts "boxLx: $boxLx; boxLy: $boxLy; boxLz: $boxLz"
set boxL [list $boxLx $boxLy $boxLz]
## get system min max
lassign [::cgfactory::tools::system::measure_minmax $molId $selection_str] xmin ymin zmin xmax ymax zmax

set axes {"x" "y"}
set n_dim 2
set mins [list $xmin $ymin $zmin]
set maxs [list $xmax $ymax $zmax]

# set 
set cc_bond_length [::cgfactory::graphene::get_cc_bond_length]
set half_band_width [expr 0.5*$cc_bond_length + $tol]

for {set i 0} {$i < $n_dim} {incr i} {
	set pbc_axis [lindex $axes $i]
	set min_position [lindex $mins $i]
	set max_position [lindex $maxs $i]
	
	set target_selection_str [::cgfactory::tools::select::band_str $pbc_axis $min_position $half_band_width $selection_str]
	set target [atomselect $molId $target_selection_str]
	set atomIds_target [$target get index]
	
	set neighbor_selection_str [::cgfactory::tools::select::band_str $pbc_axis $max_position $half_band_width $selection_str]
	set neighbor [atomselect $molId $neighbor_selection_str]
	set atomIds_neighbor [$neighbor get index]
	
	::cgfactory::tools::pbbc::setup_pbbc $molId $atomIds_target $atomIds_neighbor $boxL  $bond_length_cutoff $bond_type $tol
}


# Save pdb/psf
::cgfactory::tools::io::save_pdb $output_dir $output_prefix $molId
::cgfactory::tools::io::save_psf $output_dir $output_prefix $molId

exit



