#====================================================
## Test: testing PBBC
# Author: Yuhang Wang
# Date: 11/21/2014

#====================================================
set ext_dir "/Users/stevenw/GitCode"
lappend auto_path $ext_dir

package require cgfactory
namespace import ::cgfactory::tools
namespace import ::cgfactory::graphene

set output_dir "/Users/stevenw/Downloads"
set output_prefix "pbbc"
set resid_start 2
set segname_prefix "GC"
set chainId "C"
set Lx 20
set Ly 20
set Lz 4.0
set molId [graphene build_graphene $Lx $Ly $Lz $resid_start $segname_prefix $chainId]

set bond_length_cutoff $::cgfactory::graphene::CC_BOND_LENGTH
set target_object_selection_str "segname GC1 and name CGC"
set bond_type "CGC-CGC"
set angle_type "CGC-CGC-CGC"

set pbbc_axes {"x" "y"}
set cc_bond_length [::cgfactory::graphene::get_cc_bond_length]
set tol 0.2
set half_width [expr 0.5*$cc_bond_length + $tol]

::cgfactory::tools::pbbc::ez_add_pbbc $molId $pbbc_axes $target_object_selection_str $half_width $bond_length_cutoff $bond_type $tol $angle_type


set forbidden_angles $::cgfactory::graphene::BAD_ANGLE_DEGREES


# Save pdb/psf
::cgfactory::tools::io::save_pdb $output_dir $output_prefix $molId
::cgfactory::tools::io::save_psf $output_dir $output_prefix $molId

exit



