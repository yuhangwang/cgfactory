#====================================================
## ::cgfactory::constants
# Define constants
# Author: Yuhang Wang
# Date: 11/19/2014

#====================================================

namespace eval ::cgfactory::constants::MASS {
	variable C [expr 12.01100]	
	proc get_mass_C_bead {n_carbons} {
		## Role: return the mass of a CG carbon bead with "n_carbons" carbon atoms
		return [expr $::cgfactory::constants::MASS::C*$n_carbons]	
	}
	variable PE_HEAD_BEAD [expr 110.0]
	variable O [expr 15.999]
	variable H [expr 1.008]
}
