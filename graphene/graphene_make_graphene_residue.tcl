#====================================================
## ::cgfactory::graphene::make_graphene_residue
# Goal: make one graphene residue
# Author: Yuhang Wang
# Date: 11/19/2014

#====================================================


source [file join [::cgfactory::get_graphene_dir] "graphene_make_graphene_building_block.tcl"]

proc ::cgfactory::graphene::make_graphene_residue {N_building_blocks new_resid new_resname new_segname new_chainId} {
	## Role: make a graphene residue (one row of carbons)
	set Nx $N_building_blocks
	set Ny [expr 1]
	set Nz [expr 1]
	lassign [::cgfactory::graphene::make_graphene_building_block] molId
	
	## replicate along X
	set newMolId [::cgfactory::tools::molecule::replicate $molId $Nx $Ny $Nz]
	
	## update resid, segname
	set selection_str "all"
	::cgfactory::tools::param::set_residue_param $newMolId $selection_str $new_resid $new_resname $new_segname $new_chainId
	
	## update PBC box sizes
	::cgfactory::graphene::set_pbc_box_size $newMolId
	
	return [list $newMolId]
}