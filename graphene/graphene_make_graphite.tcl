#====================================================
## ::cgfactory::graphene::make_graphene_make_graphite
# Goal: make a few-layer graphite
# Author: Yuhang Wang
# Date: 11/19/2014

#====================================================

source [file join [::cgfactory::get_graphene_dir] "graphene_make_graphene_sheet.tcl"]

proc ::cgfactory::graphene::make_graphite {N_blocks_per_residue N_residues_per_layer N_layers resid_start new_resname new_segname_prefix new_chainId {retype_angles 1} {retype_dihedrals 1} } {
	## Role: build graphite with many layers
	set molIdList {} ;#< a list molId's; one for each layer
	lassign [list 0 0 0] dx dy dz;#< initialize translation mangnitudes
	for {set i_layer 1} {$i_layer <= $N_layers} {incr i_layer} {
		set new_segname "${new_segname_prefix}${i_layer}"
		lassign [::cgfactory::graphene::make_graphene_sheet \
		 $N_blocks_per_residue $N_residues_per_layer $resid_start \
		 $new_resname $new_segname $new_chainId \
		 $retype_angles $retype_dihedrals \
		] molId
		lappend molIdList $molId
		
		# There is no need to shift the layers.
		# Pi-Pi stacking is more favorable.
		set dx [expr 0.0]
		set dz [expr $i_layer*$::cgfactory::graphene::INTER_GRAPHENE_LAYER_DISTANCE]
		::cgfactory::tools::coordinate::move $molId $dx $dy $dz
		incr resid_start $N_residues_per_layer
	}
	
	## merge all residue into one molecule
	set newMolId [::cgfactory::tools::molecule::mergemols $molIdList]
	

	set X_extra_margin [expr 0]
	set Y_extra_margin [expr 0]
	set Z_extra_margin [expr 0]
	::cgfactory::graphene::set_pbc_box_size $newMolId $X_extra_margin $Y_extra_margin $Z_extra_margin
	
	## garbage collect un-needed intermediate molecules
	::cgfactory::tools::molecule::delete_mols $molIdList
	
	return [list $newMolId]
}


##Depreicated code

#		# For odd layers, shift along X by half C-C bond length
#		if {[expr $i_layer%2] == 1} {
#			set dx [expr 0.5*$::cgfactory::graphene::CC_BOND_LENGTH]
#		} else {
#			set dx [expr 0.0]
#		}

## Update PBC box sizes
#if {$N_layers == 1 } {
#	set X_extra_margin [expr 0.0]
#} else {
#	## Developer's note:
#	#  In multi-layer graphene, there is an extra 0.5*CC_BOND_LENGTH along X.
#	#  When using graphene::set_pbc_box_size, this extra margin will be counted
#	#  (because graphene::set_box_size measure system size first and use it 
#	#  to determine the pbc box size).
#	#  Thus we need to subtract off this extra margin.
#	set X_extra_margin [expr -0.5*$::cgfactory::graphene::CC_BOND_LENGTH]
#}