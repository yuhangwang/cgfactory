#====================================================
## ::cgfactory::graphene::make_graphene_sheet
# Goal: make one graphene sheet
# Author: Yuhang Wang
# Date: 11/19/2014

#====================================================
source [file join [::cgfactory::get_graphene_dir] "graphene_make_graphene_residue.tcl"]


proc ::cgfactory::graphene::make_graphene_sheet {N_blocks_per_residue N_residues resid_start new_resname new_segname new_chainId {retype_angles 1} {retype_dihedrals 1} } {
	## Role: make a graphene sheet 
	#  Inputs:
	#	N_blocks_per_residue: number of building blocks per residue
	#	N_residues: number of residues
	
	puts "====== N blocks per residue: $N_blocks_per_residue "
	set N_beads_per_residue [expr 2*$N_blocks_per_residue]
	set molIdList {} ;#< a list of molId's for the new molecules to be created
	lassign [list 0 0 0] dx dy dz
	set type_C $::cgfactory::graphene::TYPE::C
	set dihedral_type_name "${type_C}-${type_C}-${type_C}-${type_C}"
	set ccc_resid $resid_start ;# resid counter
		
	for {set ccc 0} {$ccc < $N_residues} {incr ccc} {
		set new_resid  $ccc_resid
		lassign [::cgfactory::graphene::make_graphene_residue \
		 $N_blocks_per_residue $new_resid $new_resname $new_segname $new_chainId] molId 
		
		## translate the new molecule along Y
		# For odd row, also shift along X by half C-C bond length
		set dy [expr $ccc*$::cgfactory::graphene::INTER_ROW_DISTANCE]
		if {[expr $ccc%2] == 1} {
			set dx [expr 0.5*$::cgfactory::graphene::CC_BOND_LENGTH]
		} else {
			set dx [expr 0.0]
		}
		::cgfactory::tools::coordinate::move $molId $dx $dy $dz
		
		lappend molIdList $molId
		incr ccc_resid
	}
	
	## merge all residue into one molecule
	set newMolId [::cgfactory::tools::molecule::mergemols $molIdList]
	
	## update PBC box sizes
	::cgfactory::graphene::set_pbc_box_size $newMolId
	
	## guess bonds
	::cgfactory::tools::topology::guess_bonds $newMolId
	
	## guess angles
	::cgfactory::tools::topology::guess_angles $newMolId
	if {$retype_angles} {
		::cgfactory::tools::topology::retype_angles_by_degrees $newMolId
	}
	# remove bad angles
	set bad_angle_types $::cgfactory::graphene::BAD_ANGLE_TYPES
	::cgfactory::tools::topology::delete_angle_types $newMolId $bad_angle_types
		
#	## add dihedrals
#	puts "add new dihedrals"
#	::cgfactory::tools::topology::add_graphene_dihedrals $newMolId $N_beads_per_residue $N_residues $dihedral_type_name
#	
	
#	## guess dihedrals
#	::cgfactory::tools::topology::guess_dihedrals $newMolId
#	# remove duplicates
#	::cgfactory::tools::topology::rm_dihedral_duplicates $newMolId
#	if {$retype_dihedrals} {
#        	# retype dihedral type names based on their angle degrees
#        	::cgfactory::tools::topology::retype_dihedrals_by_degrees $newMolId
#	}
#	
	## note: with my patch under cgfactory/patches/topotools/topodihedrals.tcl
	# there will never be dihedrals with the same 1st and 4th atom
	# Thus there is no need to call the following function.
#	::cgfactory::graphene::remove_dihedrals_with_same_1_4 $newMolId
		
		
		
	
	## garbage collect un-needed intermediate molecules
	::cgfactory::tools::molecule::delete_mols $molIdList
	
	return $newMolId	
}
