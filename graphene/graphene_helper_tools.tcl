#====================================================
## ::cgfactory::graphene
# Goal: provide helper tools for building CG graphene
# Author: Yuhang Wang
# Date: 11/18/2014
#====================================================

#==================================================
# Helper proc's
#==================================================

proc ::cgfactory::graphene::unit_box_size {} {
	## Role: return the size of the unit box
	#	for the graphene building block
	#	and also the length along X per carbon
	set bond_length $::cgfactory::graphene::CC_BOND_LENGTH
	set Lx [expr $::cgfactory::graphene::CC_BOND_LENGTH] ;# note: there is a 4.0 Angs. margin
	set Ly $::cgfactory::graphene::INTER_ROW_DISTANCE
	set Lz $::cgfactory::graphene::INTER_GRAPHENE_LAYER_DISTANCE
	set n_carbons_per_unit [expr 1]
	set Lx_per_carbon [expr $Lx/$n_carbons_per_unit] ; # ignore the margin
	return [list $Lx $Ly $Lz $Lx_per_carbon $n_carbons_per_unit]
}

proc ::cgfactory::graphene::get_NxNyNz {Lx Ly Lz} {
	## Role: get the number of graphene units along X and Y
	#	and the number of layers along Z
	lassign [::cgfactory::graphene::unit_box_size] unitLx unitLy unitLz _1 _2
	set Nx [::tcl::mathfunc::floor [expr $Lx/$unitLx]]
	set Ny [::tcl::mathfunc::floor [expr $Ly/$unitLy]]
	set Nz [::tcl::mathfunc::floor [expr $Lz/$unitLz]]
	set Nx [::tcl::mathfunc::int $Nx]
	set Ny [::tcl::mathfunc::int $Ny]
	set Nz [::tcl::mathfunc::int $Nz]
	## make sure there is at least 1 layer of graphene
	if {$Nz == 0} {set Nz 1}
	# note: the number of residues (linear grahene beads) must be even,
	#  otherwise the periodic condition will be broken.
	if {[expr $Ny%2]==1} {incr Ny}
	return [list $Nx $Ny $Nz]
}


proc ::cgfactory::graphene::set_pbc_box_size {molId {X_extra_margin 0.0} {Y_extra_margin 0.0} {Z_extra_margin 0.0} } {
	## Role: set up the PBC box size for graphene-like crystals
	#  Inputs:
	#	moldId: VMD mol ID
	#	X_extra_margin: extra margin along X
	#	Y_extra_margin: extra margin along Y
	#	Z_extra_margin: extra margin along Z
	#
	
	## Developer's note: 
	#  When measuring minmax of the system, there is an extra 1/2 CC_BOND_LENGTH added
	#  to the system dimension along X due to the Zig-Zag shape of the edge.
	#  We need to take this into account. That where we only need to set default_scale_factor
	#  to 0.5 (instead of 1.0).
	set X_scale_factor [expr 0.5]
	set x_margin [expr $X_scale_factor*$::cgfactory::graphene::CC_BOND_LENGTH + $X_extra_margin]
	
	## Developer's note:
	#  Since the edge is flat along Y, we need to add full $CC_BOND_LENGTH
	set y_margin [expr $::cgfactory::graphene::INTER_ROW_DISTANCE + $Y_extra_margin]
	set z_margin [expr $::cgfactory::graphene::INTER_GRAPHENE_LAYER_DISTANCE + $Z_extra_margin]
	lassign [::cgfactory::tools::pbc::smart_set_box_size $molId $x_margin $y_margin $z_margin] Lx Ly Lz
	return [list $Lx $Ly $Lz]
}


proc ::cgfactory::graphene::remove_dihedrals_with_same_1_4 {molId} {
	## Role: remove dihedrals whose 1st and 4th atom are the same
	#  Input:
	#	molId: VMD molecluar ID
	puts "== remove dihedrals with same 1st and 4th atom"
	set all_dihedrals [::cgfactory::tools::topology::get_dihedral_list $molId]
	foreach _dihe $all_dihedrals {
		lassign $_dihe dihe_type id1 id2 id3 id4
		if {$id1 == $id4} {
			puts "delete dihedrals with same 1, 4 atoms: $dihe_type $id1 $id2 $id3 $id4"
			::cgfactory::tools::topology::delete_dihedral $molId $id1 $id2 $id3 $id4
		}
	}
}


proc ::cgfactory::graphene::remove_bad_gox_dihedrals {molId selection_str} {
	## Role: remove bad dihedrals in graphene-oxide like
	#	GOH-CGC-CGC-GOH
	#  Input:
	#	molId: VMD molecluar ID
	puts "=== Remove bad dihedrals in Graphene-Oxide ==="
	set bad_dihedral_types {"GOH-CGC-CGC-GOH"}
	set all_dihedrals [::cgfactory::tools::topology::get_dihedral_list $molId $selection_str]
	foreach _dihe $all_dihedrals {
		lassign $_dihe dihe_type id1 id2 id3 id4
		if {[lsearch $bad_dihedral_types $dihe_type] != -1 } {
			puts "=== delete bad dihedrals in graphene-oxide: $dihe_type $id1 $id2 $id3 $id4"
			::cgfactory::tools::topology::delete_dihedral $molId $id1 $id2 $id3 $id4
		}
	}
}