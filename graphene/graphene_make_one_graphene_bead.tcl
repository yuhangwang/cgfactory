#====================================================
## ::cgfactory::tools::make_one_graphene_bead
# Goal: make one CG graphene bead
# Author: Yuhang Wang
# Date: 11/19/2014

#====================================================

proc ::cgfactory::graphene::make_one_graphene_bead {} {
	## Role: build a single CG graphene carbon atom
	set molId [::cgfactory::tools::make::make_one_bead]
	set oneAtom [atomselect $molId all]
	$oneAtom set type   $::cgfactory::graphene::TYPE::C
	$oneAtom set name   $::cgfactory::graphene::NAME::C
	$oneAtom set mass   $::cgfactory::graphene::MASS::C
	$oneAtom set charge $::cgfactory::graphene::CHARGE::C
	$oneAtom set radius $::cgfactory::graphene::CC_BOND_LENGTH
	return [list $molId $oneAtom]
}