
#proc ::cgfactory::graphene::renumber {molId {segname_prefix "G"}} {
#	## Role: renumber the atom index, residue Id and segment Id
#	# input: 
#	# :param molId: molecule ID used by VMD for this molecule
#	# :param segname_prefix: prefix for the segname to used for each layer
#	#		default: "G". The results will be like "G1", "G2", ...
#	# conventions: 
#	# 	each row (along X) is a residue
#	# 	atom is traversed row-wise
#	#	each layer is one segment
#	#-----------------------------------------------------------
#	set tol [expr 0.5] ;#< distance tolerance (i.e. accuracy)
#	
#	## measure system
#	lassign [::cgfactory::graphene::measure_system $molId] \
#		xmin ymin zmin xmax ymax zmax Lx Ly Lz
#	
#	## Find the number of atoms along X, Y, Z
#	# 	then the totla number of atoms = nX*nY*nZ
#	set nX [::cgfactory::graphene::get_n_atoms_XY $molId "y" $ymin $zmin $tol]
#	set nY [::cgfactory::graphene::get_n_atoms_XY $molId "x" $xmin $zmin $tol]
#	set nZ [::cgfactory::graphene::get_n_atoms_Z  $molId $xmin $ymin $tol]
#	
#	set minIncr [expr 0.2];#< minimum increment
#	# developer note: use max function to avoid setting deltaX/Y/Z to 0 
#	# in certain cases where there is only 1 layer of atoms and Lx/Ly/Lz = 0
#	set dx [expr $Lx/$nX]
#	set dy [expr $Ly/$nY]
#	set dz [expr $Lz/$nZ]
#	set deltaX [expr max($minIncr, $dx)] ;#< scanning step size along X
#	set deltaY [expr max($minIncr, $dy)] ;#< scanning step size along Y
#	set deltaZ [expr max($minIncr, $dz)] ;#< scanning step size along Z
#	puts "delta: $deltaX $deltaY $deltaZ"
#	
#	## initialize id's
#	set id_residue 	[expr 1]
#	set id_atom 	[expr 1]
#	set id_segment	[expr 1]
#	
#	## renumber
#	# each row is a residue
#	# each layer is a segment
#	puts "looping .."
#	puts "zmin zmax: $zmin $zmax"
#	for {set _z $zmin} {$_z < [expr $zmax+$tol]} {set _z [expr $_z + $deltaZ]} {
#		puts "z ..."
#		for {set _y $ymin} {$_y < $ymax} {set _y [expr $_y + $deltaY]} {
#			for {set _x $xmin} {$_x < [expr $xmax+$tol]} {set _x [expr $_x + $deltaX]} {
#				set new_segname "${segname_prefix}${id_segment}"
#				set new_resid 	$id_residue
#				lassign [::cgfactory::graphene::get_tol_range $_x $tol] x_upper x_lower
#				lassign [::cgfactory::graphene::get_tol_range $_y $tol] y_upper y_lower
#				lassign [::cgfactory::graphene::get_tol_range $_z $tol] z_upper z_lower
#				set sel_X [::cgfactory::graphene::make_axis_range_selection_txt "x" $x_upper $x_lower]
#				set sel_Y [::cgfactory::graphene::make_axis_range_selection_txt "y" $y_upper $y_lower]
#				set sel_Z [::cgfactory::graphene::make_axis_range_selection_txt "z" $z_upper $z_lower]
#				set sel_str "($sel_X) and ($sel_Y) and ($sel_Z) "
#				set _sel [atomselect $molId "$sel_str"]
#				puts "number of selected atoms: [$_sel num]"
#				$_sel set resid   $new_resid
#				$_sel set segname $new_segname
#				puts [$_sel get segname]
#				$_sel delete
#				incr id_atom
#			}
#			incr id_residue
#		}
#		incr id_segment
#	}
#	
#	return
#}


proc ::cgfactory::graphene::get_tol_range {center tol} {
	## Role: return a list [center-tol, center+tol]
	set lower [expr $center - $tol]
	set upper [expr $center + $tol]
	return [list $lower $upper]
}

proc ::cgfactory::graphene::make_axis_range_selection_txt {axis lower upper} {
	## Role: make a selection range along one axis
	return "($axis > $lower and $axis < $upper)"
}

proc ::cgfactory::graphene::get_n_atoms_XY {molId axis min zmin {tol 0.2}} {
	## Role: return the number of atoms along X or Y
	# inputs:
	#  molId: VMD molecule ID
	#  axis: e.g. "x", "y", or "z"
	#  min: minimum along X, Y or Z
	#  tol: accuracy tolerance (default: 0.2)
	set _min_lower [expr $min - $tol]
	set _min_upper [expr $min + $tol]
	set _zmin_lower [expr $zmin - $tol]
	set _zmin_upper [expr $zmin + $tol]
	set extra_sel "(z>$_zmin_lower) and (z<$_zmin_upper)"
	set _sel [atomselect $molId "($axis > $_min_lower) and ($axis < $_min_upper) and ($extra_sel)"]
	set n_atoms [$_sel num]
	$_sel delete
	return $n_atoms
}

proc ::cgfactory::graphene::get_n_atoms_Z {molId xmin ymin {tol 0.2}} {
	## Role: return the number of atoms along Z
	# inputs:
	#  molId: VMD molecule ID
	#  xmin: minimum along X
	#  ymin: minimum along Y
	#  tol: accuracy tolerance (default: 0.2)
	set _xmin_lower [expr $xmin - $tol]
	set _xmin_upper [expr $xmin + $tol]
	set _ymin_lower [expr $ymin - $tol]
	set _ymin_upper [expr $ymin + $tol]
	set sel1 "(x>$_xmin_lower) and (x<$_xmin_upper)"
	set sel2 "(y>$_ymin_lower) and (y<$_ymin_upper)"
	set _sel [atomselect $molId "($sel1) and ($sel2)"]
	set n_atoms [$_sel num]
	$_sel delete
	return $n_atoms
}