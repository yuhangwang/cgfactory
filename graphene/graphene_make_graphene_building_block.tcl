#====================================================
## ::cgfactory::graphene::make_graphene_building_block
# Goal: make one graphene building block
# Author: Yuhang Wang
# Date: 11/19/2014

#====================================================
source [file join [::cgfactory::get_graphene_dir] "graphene_make_one_graphene_bead.tcl"]

proc ::cgfactory::graphene::make_graphene_building_block {} {
	## get the builing block for CG graphene
	lassign [::cgfactory::graphene::make_one_graphene_bead] molId oneAtom
	lassign [::cgfactory::graphene::unit_box_size] Lx Ly Lz Lx_per_carbon
	::cgfactory::tools::pbc set_box_size $molId $Lx_per_carbon $Ly $Lz
	
	## user two carbon atoms in each building block (along X)
	lassign [::cgfactory::graphene::unit_box_size] Lx Ly Lz Lx_per_carbon n_carbonds_per_unit
	set Nx $n_carbonds_per_unit
	set Ny [expr 1]
	set Nz [expr 1]
	set newMolId [::cgfactory::tools::molecule::replicate $molId $Nx $Ny $Nz]
	
	## update PBC box sizes
	::cgfactory::tools::pbc::set_box_size $newMolId $Lx $Ly $Lz
	
	return [list $newMolId]
}
