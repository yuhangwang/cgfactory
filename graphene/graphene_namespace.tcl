# Role: provide namespaces specific to ::cgfactory::graphene
# Author: Yuhang Wang
# Date: 11/17/2014


namespace eval ::cgfactory::graphene {
	variable N_CARBONS_PER_BEAD [expr 6] ;# Min et. al. Soft Matter 8:8735 (2012)
	variable N_BEADS_PER_BLOCK  [expr 2]
	variable CC_BOND_LENGTH     [expr 4.26] ;# Min et. al. Soft Matter 8:8735 (2012)
	variable INTER_GRAPHENE_LAYER_DISTANCE [expr 3.4] ;# reference: Li et. al. PNAS 2013, VOL. 110 12295--12300
	
	# distance between neighboring rows
	set Pi [expr 3.1415926]
	set graphene_angle [expr 60.*$Pi/180.] ;# 60 degrees
	variable INTER_ROW_DISTANCE [expr $CC_BOND_LENGTH*sin($graphene_angle)]
	
	proc get_cc_bond_length {} {
		return $::cgfactory::graphene::CC_BOND_LENGTH
	}
}

namespace eval ::cgfactory::graphene::TYPE {
	# Role: define conventions for atom types, names in CG graphene
	variable atomTypeDict {"C" "CGC"}
	variable C [dict get $atomTypeDict "C"]
}

namespace eval ::cgfactory::graphene::NAME {
	# Role: define conventions for atom types, names in CG graphene
	variable atomNameDict {"C" "CGC"}
	variable C [dict get $atomNameDict "C"]
}

namespace eval ::cgfactory::graphene::MASS {
	# Role: conventions for atom masses
	set CG_CarbonMass [::cgfactory::constants::MASS::get_mass_C_bead $::cgfactory::graphene::N_CARBONS_PER_BEAD]
	variable atomMassDict [list "C" $CG_CarbonMass]
	variable C [dict get $atomMassDict "C"]
}

namespace eval ::cgfactory::graphene::CHARGE {
	# Role: conventions for atom masses
	variable atomChargeDict {"C" 0.0}
	variable C [dict get $atomChargeDict "C"]
}

namespace eval ::cgfactory::graphene {
	set OH $::cgfactory::oxidize::TYPE::OH
	set CG $::cgfactory::graphene::TYPE::C
	variable BAD_DIHEDRAL_TYPES [list "${OH}-${CG}-${CG}-${OH}"]
	variable BAD_ANGLE_TYPES [list "${CG}-${CG}-${CG}-120"]
	variable BAD_ANGLE_DEGREES [list 120]
}

