#====================================================
## ::cgfactory::oxidize::build_graphene_oxide
# Goal: build coarse-grained graphene-oxide with 
#	oxygen speices on top|bottom|both graphene 
#	layers.
# Author: Yuhang Wang
# Date: 11/20/2014

#====================================================
source [file join [::cgfactory::get_graphene_dir] "build_graphene.tcl"]
source [file join [::cgfactory::get_oxidize_dir] "oxidize.tcl"]

proc ::cgfactory::graphene::build_graphene_oxide {Lx Ly Lz topBottom oxy_level {resid_start 1} {segname_prefix "G"} {new_chainId "G"}} {
	## Role: build coarse-grained graphene-oxide
	#  Inputs:
	#	topBottom: 1 for oxiding the top; -1 for oxidizing the bottom; 2 for oxidizing both top and bottom layers
	#	output_dir: output directory
	#	output_prefix: output file prefix
	#	resid_start: starting residue ID (default: 1)
	#	segname_prefix: segment name prefix (default: "G")
	#	new_chainId: chain ID (default: "G")
	#
	puts "building graphene-oxide ..."
	set resname "GRA"
	lassign [::cgfactory::graphene::get_NxNyNz $Lx $Ly $Lz] Nx Ny Nz

	## build graphene sheet
	set retype_angles 1
	set retype_dihedrals 1
	lassign [::cgfactory::graphene::make_graphite \
        	 $Nx $Ny $Nz $resid_start $resname $segname_prefix $new_chainId \
        	 $retype_angles $retype_dihedrals ] molId
	
	## define the segment numbering
	set seg_num_one [expr 1]
	set seg_num_end [expr $Nz]
	
	set molsToBeDeleted {}

	## prevent user from oxidize both sides of a single layer graphene
	if {$Nz == 1 && $topBottom == 2 } {
		puts "ERROR HINT: you are trying to oxidize a single layer of graphene on both sides. Abort ..."
		puts "Try to choose topBottom to be \"-1\" or \"1\" instead"
		exit
	}
	
	set Z_extra_margin [expr 0] ;# extra margin along Z due the addition of oxidation species
	
	## a list of atom selections that are oxidized
	set oxy_selection_list {}
	
	## add CG hydroxy groups to bottom graphene layer
	if {$topBottom == 2 || $topBottom == -1 } {
		set layer_segname "${segname_prefix}${seg_num_one}"
    		set selection_str "segname $layer_segname and (name $::cgfactory::graphene::NAME::C)"
		set upDown -1
		lappend molsToBeDeleted $molId
		set molId [::cgfactory::oxidize::add_cg_hydroxyl $molId $selection_str $upDown $oxy_level]
		set Z_extra_margin [expr $Z_extra_margin + $::cgfactory::oxidize::CO_BOND_LENGTH]
		lappend oxy_selection_list "(segname ${layer_segname})"
	} 
	
	## add CG hydroxy groups to top graphene layer
	if {$topBottom == 2 || $topBottom == 1 } {
		set layer_segname "${segname_prefix}${seg_num_end}"
    		set selection_str "segname ${layer_segname} and (name $::cgfactory::graphene::NAME::C)"
		set upDown 1
		lappend molsToBeDeleted $molId
		set molId [::cgfactory::oxidize::add_cg_hydroxyl $molId $selection_str $upDown $oxy_level]
		set Z_extra_margin [expr $Z_extra_margin + $::cgfactory::oxidize::CO_BOND_LENGTH]
		lappend oxy_selection_list "(segname ${layer_segname})"
	}
	
	set sel_str_oxidized_layers [join $oxy_selection_list " or "]
	
	## === setup new PBC box ===
	lassign [::cgfactory::tools::pbc::get_box_size $molId] Lx Ly Lz
	set Lz [expr $Lz + $Z_extra_margin]
	::cgfactory::tools::pbc::set_box_size $molId $Lx $Ly $Lz

	## === add angles to the oxidized layers ===
#	::cgfactory::tools::topology::guess_angles $molId $sel_str_oxidized_layers
#	::cgfactory::tools::topology::retype_angles_by_degrees $molId $sel_str_oxidized_layers
#	set bad_angle_types $::cgfactory::graphene::BAD_ANGLE_TYPES
#	::cgfactory::tools::topology::delete_angle_types $molId $bad_angle_types $sel_str_oxidized_layers
	
	## === Add new dihedrals ===
	# add new dihedrals involving OHG
	set sel_str_OH "name ${::cgfactory::oxidize::NAME::OH}"
	set C $::cgfactory::graphene::NAME::C
	set O $::cgfactory::oxidize::NAME::OH
	set new_dihedral_type [join [list $O $C $C $C] "-"]
	::cgfactory::tools::topology::add_graphene_oxide_dihedrals $molId $sel_str_OH $new_dihedral_type

	
	## === recenter the molecule to be around the origin ===
	::cgfactory::tools::coordinate::recenter $molId
	
	
	## === garbage collect ===
	::cgfactory::tools::molecule::delete_mols $molsToBeDeleted ;#< delete old molecule
	
	return $molId
}