#====================================================
## ::cgfactory::oxidize::build_graphene_oxide
# Goal: build coarse-grained graphene-oxide with 
#	oxygen speices on top|bottom|both graphene 
#	layers.
# Author: Yuhang Wang
# Date: 11/20/2014

#====================================================
source [file join [::cgfactory::get_graphene_dir] "build_graphene.tcl"]
source [file join [::cgfactory::get_oxidize_dir] "oxidize.tcl"]

proc ::cgfactory::graphene::build_graphene_with_hydrophilic_mutations {Lx Ly Lz topBottom percent_of_mutations {resid_start 1} {segname_prefix "G"} {new_chainId "G"}} {
	## Role: build coarse-grained graphene-oxide
	#  Inputs:
	#	Lx: length of the simiulation box along X
	#	Ly: length of the simiulation box along Y
	#	Lz: length of the simiulation box along Z
	#	topBottom: 1 for oxiding the top; -1 for oxidizing the bottom; 2 for oxidizing both top and bottom layers
	#	percent_of_mutations: percentage of the graphene layers to be mutated
	#	resid_start: starting residue ID (default: 1)
	#	segname_prefix: segment name prefix (default: "G")
	#	new_chainId: chain ID (default: "G")
	#
	puts "building graphene with hydrophilic beads ..."
	set resname "GRA"
	lassign [::cgfactory::graphene::get_NxNyNz $Lx $Ly $Lz] Nx Ny Nz

	## build graphene sheet
	set retype_angles 1
	set retype_dihedrals 1
	lassign [::cgfactory::graphene::make_graphite \
        	 $Nx $Ny $Nz $resid_start $resname $segname_prefix $new_chainId \
        	 $retype_angles $retype_dihedrals ] molId
	
	## define the segment numbering
	set seg_num_one [expr 1]
	set seg_num_end [expr $Nz]
	
	## prevent user from oxidize both sides of a single layer graphene
	if {$Nz == 1 && $topBottom == 2 } {
		puts "ERROR HINT: you are trying to oxidize a single layer of graphene on both sides. Abort ..."
		puts "Try to choose topBottom to be \"-1\" or \"1\" instead"
		exit
	}
	
	set Z_extra_margin [expr 0] ;# extra margin along Z due the addition of oxidation species
	
	## a list of atom selections that are oxidized
	set oxy_selection_list {}
	
	## add CG hydroxy groups to bottom graphene layer
	if {$topBottom == 2 || $topBottom == -1 } {
		set layer_segname "${segname_prefix}${seg_num_one}"
    		set selection_str "segname $layer_segname and (name $::cgfactory::graphene::NAME::C)"
		set upDown -1
		::cgfactory::oxidize::mutate_to_hydrophilic $molId $selection_str $upDown $percent_of_mutations
	} 
	
	## add CG hydroxy groups to top graphene layer
	if {$topBottom == 2 || $topBottom == 1 } {
		set layer_segname "${segname_prefix}${seg_num_end}"
    		set selection_str "segname ${layer_segname} and (name $::cgfactory::graphene::NAME::C)"
		set upDown 1
		::cgfactory::oxidize::mutate_to_hydrophilic $molId $selection_str $upDown $percent_of_mutations
	}
	
	## === recenter the molecule to be around the origin ===
	::cgfactory::tools::coordinate::recenter $molId
	
	return $molId
}