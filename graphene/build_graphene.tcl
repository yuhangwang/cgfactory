#====================================================
## ::cgfactory::graphene
# Goal: build a coarse-grained graphene sheet
# Author: Yuhang Wang

#====================================================


## import helper tools
source [file join [::cgfactory::get_graphene_dir] "graphene_helper_tools.tcl"]
## import proc for building graphite
source [file join [::cgfactory::get_graphene_dir] "graphene_make_graphite.tcl"]

proc ::cgfactory::graphene::build_graphene {Lx Ly Lz {resid_start 1} {segname_prefix "G"} {new_chainId "G"} {retype_angles 1} {retype_dihedrals 1} } {
	## calculate the number of building blocks to be replacted along X, Y, and Z
	#  Inputs:
	#	Lx: length along X
	#	Ly: length along Y
	#	Lz: length along Z
	#	output_dir: output directory
	#	output_prefix: output file prefix
	#	resid_start: starting residue ID (default: 1)
	#	segname_prefix: segment name prefix (default: "G")
	#	new_chainId: chain ID (default: "G")
	#
	
	set resname "GRA"
	lassign [::cgfactory::graphene::get_NxNyNz $Lx $Ly $Lz] Nx Ny Nz
	
	## build graphene sheet
	lassign [::cgfactory::graphene::make_graphite \
	 $Nx $Ny $Nz $resid_start $resname $segname_prefix $new_chainId $retype_angles $retype_dihedrals] molId
	
	
	## recenter the molecule to be around the origin
	::cgfactory::tools::coordinate::recenter $molId
	
	lassign [::cgfactory::tools::pbc::get_box_size $molId] Lx Ly Lz
#	puts "build graphene: new box Lx: $Lx; box Ly: $Ly;  box Lz: $Lz"
	
	lassign [::cgfactory::tools::system::measure_length $molId "all"] rawLx rawLy rawLz
#	puts "build graphene: raw Lx: $rawLx; raw Ly: $rawLy; raw Lz: $rawLz"
	
	return [list $molId]
}
